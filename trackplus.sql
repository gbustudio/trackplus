/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : localhost
 Source Database       : trackplus

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : utf-8

 Date: 05/19/2016 16:51:56 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `tpAdminAccessLogs`
-- ----------------------------
DROP TABLE IF EXISTS `tpAdminAccessLogs`;
CREATE TABLE `tpAdminAccessLogs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminId` bigint(20) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `ipAddress` varchar(128) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `adminId` (`adminId`),
  CONSTRAINT `tpadminaccesslogs_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `tpAdmins` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpAdminAccessLogs`
-- ----------------------------
BEGIN;
INSERT INTO `tpAdminAccessLogs` VALUES ('1', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', '127.0.0.1', '2016-03-01 14:59:29'), ('2', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', '127.0.0.1', '2016-03-02 00:14:32'), ('3', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', '127.0.0.1', '2016-03-11 00:05:36'), ('4', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '127.0.0.1', '2016-04-18 10:23:58'), ('5', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '127.0.0.1', '2016-04-18 10:44:30'), ('6', '2', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '127.0.0.1', '2016-04-19 16:26:22'), ('7', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '127.0.0.1', '2016-04-20 15:48:24'), ('8', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '192.168.1.124', '2016-04-20 17:07:37'), ('9', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '192.168.1.134', '2016-04-20 19:33:30'), ('10', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '127.0.0.1', '2016-04-22 06:55:06'), ('11', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-04-25 17:47:57'), ('12', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-04-27 12:59:46'), ('13', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-04-28 00:51:18'), ('14', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-04-30 10:47:22'), ('15', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-04-30 13:17:44'), ('16', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-05-02 10:30:00'), ('17', '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36', '127.0.0.1', '2016-05-03 14:27:09');
COMMIT;

-- ----------------------------
--  Table structure for `tpAdminProfiles`
-- ----------------------------
DROP TABLE IF EXISTS `tpAdminProfiles`;
CREATE TABLE `tpAdminProfiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `address` varchar(255) DEFAULT '',
  `phone` varchar(255) DEFAULT '',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adminId` (`adminId`),
  CONSTRAINT `tpadminprofiles_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `tpAdmins` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpAdminProfiles`
-- ----------------------------
BEGIN;
INSERT INTO `tpAdminProfiles` VALUES ('1', '1', 'Antonius Henry Setiawan', '', '', '2016-05-19 16:50:01', '2016-03-01 13:34:22', null), ('2', '2', '', '', '', '2016-04-20 16:26:13', '2016-03-01 14:38:28', '2016-04-20 16:26:13'), ('3', '3', 'Administrator', '', '', '2016-04-20 16:26:12', '2016-04-20 16:21:51', '2016-04-20 16:26:12');
COMMIT;

-- ----------------------------
--  Table structure for `tpAdmins`
-- ----------------------------
DROP TABLE IF EXISTS `tpAdmins`;
CREATE TABLE `tpAdmins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `pwdLen` int(3) NOT NULL,
  `permission` text,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `tpadmins_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tpCompanies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpAdmins`
-- ----------------------------
BEGIN;
INSERT INTO `tpAdmins` VALUES ('1', '1', 'user1@trackplus.com', 'D648A0DF6BBD90860DC9B07B07B06D9686AFCAA8', '8', 'a:0:{}', '2016-04-19 19:14:02', '2016-03-01 13:34:22', null), ('2', '1', 'user2@trackplus.com', 'D648A0DF6BBD90860DC9B07B07B06D9686AFCAA8', '8', 'a:0:{}', '2016-04-20 16:26:13', '2016-03-01 14:38:28', '2016-04-20 16:26:13'), ('3', '1', 'admin@trackplus.com', '48DE9092A5727E4C27E625302B1FD6F17FEBFA4F', '16', null, '2016-04-20 16:26:12', '2016-04-20 16:21:51', '2016-04-20 16:26:12');
COMMIT;

-- ----------------------------
--  Table structure for `tpAnnouncements`
-- ----------------------------
DROP TABLE IF EXISTS `tpAnnouncements`;
CREATE TABLE `tpAnnouncements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `published` tinyint(1) DEFAULT NULL,
  `tasks` text,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `tpannouncements_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tpCompanies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpAnnouncements`
-- ----------------------------
BEGIN;
INSERT INTO `tpAnnouncements` VALUES ('1', '1', 'Announcement 1', '<p><span style=\"color:#00FF00\">Hello marketing,</span><br />\nThis is an announcement create by <span style=\"color:#FF0000\">Admin.</span><br />\n<br />\nCreated for testing purpose. Don&#39;t mind me.</p>\n', '1', 'a:0:{}', '2016-04-20 16:35:11', '2016-03-02 00:18:44', null), ('2', '1', 'Announcement 2', '<h3><strong>Marketing of The Month</strong></h3>\n\n<hr />\n<p><span style=\"background-color:#DDA0DD\">Lorem <span style=\"color:#FFFF00\">ipsum</span> dolor sit amet</span>, consectetur adipisicing elit. Consequatur, molestiae, eos. Eius sed nesciunt, iste ipsa, architecto et facilis esse, nulla voluptatum at dolor est iusto aperiam necessitatibus omnis illo? Lorem ipsum dolor sit amet, <cite>consectetur</cite> adipisicing elit. Eveniet, <tt>provident</tt> sit magnam assumenda incidunt adipisci ab necessitatibus commodi quisquam reprehenderit unde sequi aliquam ut, recusandae sunt totam error modi qui! Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span style=\"color:#8B4513\">Necessitatibus</span> corporis modi officiis, <var>laboriosam <small>eaque</small> consectetur</var>, quod. Laudantium quia rem dignissimos. Eius cumque, harum adipisci voluptas similique <code>cupiditate</code> incidunt commodi. Error! Lorem ipsum dolor sit amet, consectetur <ins>adipisicing</ins> elit. Tempore quisquam sapiente perferendis, recusandae reiciendis soluta in commodi explicabo quia libero <span style=\"background-color:#FFD700\">necessitatibus</span> velit repellendus nemo qui exercitationem provident odio dolor. Et. Lorem ipsum dolor sit amet, consectetur <span style=\"color:#FF0000\"><span style=\"background-color:#0000CD\">adipisicing</span></span> elit. Pariatur est facere, aut possimus non adipisci dolore nobis eos voluptas magnam <span style=\"color:#A52A2A\">dolores</span> suscipit vitae. Numquam eos eius nesciunt voluptatibus temporibus mollitia. Lorem ipsum dolor sit <span style=\"color:#FF8C00\">amet</span>, consectetur adipisicing elit. Soluta <span style=\"background-color:#00FFFF\">nisi</span> fugit fugiat maxime quo, repellat, similique dignissimos ratione, delectus dicta esse quisquam <span style=\"color:#FF8C00\">doloribus</span> aut sit ut animi a saepe ducimus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio nemo, deleniti ex, explicabo dolores animi sequi omnis. Labore aliquid sequi molestias iste alias rerum, dignissimos iusto fuga corporis eius placeat! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid laborum odio modi vel, sit placeat sed fugit, quos quod repellendus necessitatibus culpa ducimus explicabo, dignissimos porro, aspernatur dolores asperiores est?</p>\n\n<p>&nbsp;</p>\n\n<blockquote>\n<p>Everythings gonna be alright.</p>\n\n<p>No matter what happens, keep strong!</p>\n\n<p>Be Yourself!</p>\n</blockquote>\n', '1', 'a:0:{}', '2016-04-20 16:34:04', '2016-03-02 00:25:18', null), ('3', '1', 'Announcement 2', '<p>a</p>\n', '0', 'a:0:{}', '2016-04-20 16:32:17', '2016-03-11 00:05:50', null), ('4', '1', 'Announcement 4', '<p>a having b</p>\n', '0', 'a:0:{}', '2016-04-20 16:33:01', '2016-03-11 00:06:06', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpCompanies`
-- ----------------------------
DROP TABLE IF EXISTS `tpCompanies`;
CREATE TABLE `tpCompanies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpCompanies`
-- ----------------------------
BEGIN;
INSERT INTO `tpCompanies` VALUES ('1', 'Global\'s Bersama Utama', 'globals-bersama-utama1456813979', '2016-03-01 13:32:59', '2016-03-01 13:32:59', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpCompanyProfiles`
-- ----------------------------
DROP TABLE IF EXISTS `tpCompanyProfiles`;
CREATE TABLE `tpCompanyProfiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `tpcompanyprofiles_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tpCompanies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `tpCustomers`
-- ----------------------------
DROP TABLE IF EXISTS `tpCustomers`;
CREATE TABLE `tpCustomers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` text,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `cpName` varchar(20) DEFAULT NULL,
  `cpEmail` varchar(50) DEFAULT NULL,
  `cpPhone` varchar(20) DEFAULT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`companyId`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `tpcustomers_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tpcompanies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpCustomers`
-- ----------------------------
BEGIN;
INSERT INTO `tpCustomers` VALUES ('1', '1', 'Henry', 'Ploso Timur 1/110', '+6281310310927', '', '', '', '', '2016-04-30 12:13:08', '2016-04-30 11:32:25', '2016-04-30 12:13:08');
COMMIT;

-- ----------------------------
--  Table structure for `tpFormItems`
-- ----------------------------
DROP TABLE IF EXISTS `tpFormItems`;
CREATE TABLE `tpFormItems` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `formId` bigint(20) DEFAULT NULL,
  `pageId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `isCompulsory` tinyint(1) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `dataStore` text,
  `visibilityArray` text,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `formId` (`formId`),
  CONSTRAINT `tpformitems_ibfk_1` FOREIGN KEY (`formId`) REFERENCES `tpForms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpFormItems`
-- ----------------------------
BEGIN;
INSERT INTO `tpFormItems` VALUES ('1', '1', '1', 'Nama', '', 'text', '1', '1', 'a:0:{}', 'a:0:{}', '2016-03-01 15:53:57', '2016-03-01 15:53:57', null), ('2', '1', '1', 'Alamat', '', 'text', '1', '2', 'a:0:{}', 'a:0:{}', '2016-03-01 15:54:02', '2016-03-01 15:54:02', null), ('3', '1', '1', 'Kota', '', 'text', '1', '3', 'a:0:{}', 'a:0:{}', '2016-03-01 15:55:50', '2016-03-01 15:55:50', null), ('4', '3', '3', 'Full Name', 'Customer\'s Full Name', 'text', '1', '1', 'a:0:{}', 'a:0:{}', '2016-04-20 16:39:17', '2016-04-20 16:39:17', null), ('5', '3', '3', 'Address', 'Customer\'s Address', 'textarea', '1', '4', 'a:0:{}', 'a:0:{}', '2016-04-20 16:42:12', '2016-04-20 16:39:31', null), ('6', '3', '3', 'City', 'Customer\'s City', 'dropdown', '1', '5', 'a:1:{s:6:\"values\";a:11:{i:0;s:8:\"Surabaya\";i:1;s:7:\"Jakarta\";i:2;s:7:\"Bandung\";i:3;s:6:\"Madiun\";i:4;s:10:\"Purwokerto\";i:5;s:6:\"Manado\";i:6;s:11:\"Banjarmasin\";i:7;s:10:\"Balikpapan\";i:8;s:5:\"Ambon\";i:9;s:6:\"Sampit\";i:10;s:7:\"Sumenep\";}}', 'a:0:{}', '2016-04-20 16:42:12', '2016-04-20 16:41:12', null), ('7', '3', '3', 'Phone Number', 'Customer\'s Phone Number', 'text', '1', '3', 'a:0:{}', 'a:0:{}', '2016-04-20 16:42:12', '2016-04-20 16:41:42', null), ('8', '3', '3', 'Email', 'Customer\'s Business Email Address', 'text', '1', '2', 'a:0:{}', 'a:0:{}', '2016-04-20 16:42:12', '2016-04-20 16:41:58', null), ('9', '3', '4', 'Product Name', 'Name of product bought by customer', 'text', '1', '1', 'a:0:{}', 'a:0:{}', '2016-04-20 16:43:16', '2016-04-20 16:43:07', null), ('10', '3', '4', 'Rate', 'Product satisfactory rate by customer', 'dropdown', '1', '2', 'a:1:{s:6:\"values\";a:5:{i:0;s:1:\"5\";i:1;s:1:\"4\";i:2;s:1:\"3\";i:3;s:1:\"2\";i:4;s:1:\"1\";}}', 'a:0:{}', '2016-04-20 16:43:41', '2016-04-20 16:43:41', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpFormPages`
-- ----------------------------
DROP TABLE IF EXISTS `tpFormPages`;
CREATE TABLE `tpFormPages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `formId` bigint(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '1',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `formId` (`formId`),
  CONSTRAINT `tpformpages_ibfk_1` FOREIGN KEY (`formId`) REFERENCES `tpForms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpFormPages`
-- ----------------------------
BEGIN;
INSERT INTO `tpFormPages` VALUES ('1', '1', 'Page 1', '1', '2016-03-01 15:53:50', '2016-03-01 15:53:50', null), ('2', '1', 'Page 2', '2', '2016-03-01 15:55:44', '2016-03-01 15:55:44', null), ('3', '3', 'Customer Information', '1', '2016-04-20 16:36:56', '2016-04-20 16:36:56', null), ('4', '3', 'Product Information', '2', '2016-04-20 16:37:05', '2016-04-20 16:37:05', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpFormSubmissions`
-- ----------------------------
DROP TABLE IF EXISTS `tpFormSubmissions`;
CREATE TABLE `tpFormSubmissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `marketingId` bigint(20) DEFAULT NULL,
  `formId` bigint(20) DEFAULT NULL,
  `submission` text,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `formId` (`formId`),
  KEY `marketingId` (`marketingId`),
  CONSTRAINT `tpformsubmissions_ibfk_1` FOREIGN KEY (`formId`) REFERENCES `tpForms` (`id`),
  CONSTRAINT `tpformsubmissions_ibfk_2` FOREIGN KEY (`marketingId`) REFERENCES `tpMarketings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpFormSubmissions`
-- ----------------------------
BEGIN;
INSERT INTO `tpFormSubmissions` VALUES ('1', '1', '1', 'a:1:{i:1;a:3:{i:1;s:23:\"Antonius Henry Setiawan\";i:2;s:20:\"Ploso Timur 1 no 110\";i:3;s:8:\"Surabaya\";}}', '2016-04-18 11:15:33', '2016-04-18 11:15:33', null), ('2', '2', '1', 'a:1:{i:1;a:3:{i:1;s:21:\"Indriani Widyaningrum\";i:2;s:7:\"Kebraon\";i:3;s:8:\"Surabaya\";}}', '2016-04-18 11:33:42', '2016-04-18 11:33:42', null), ('3', '2', '3', 'a:2:{i:3;a:5:{i:4;s:23:\"Antonius Henry Setiawan\";i:8;s:33:\"antonius.henry.setiawan@gmail.com\";i:7;s:14:\"+6281310310927\";i:5;s:17:\"Ploso Timur 1/110\";i:6;s:8:\"Surabaya\";}i:4;a:2:{i:9;s:20:\"Microsoft Office Pro\";i:10;s:1:\"5\";}}', '2016-04-20 19:30:23', '2016-04-20 19:30:23', null), ('4', '2', '3', 'a:2:{i:3;a:5:{i:4;s:21:\"Indriani Widyaningrum\";i:8;s:18:\"indriani@gmail.com\";i:7;s:11:\"08112345678\";i:5;s:9:\"Kebraon 1\";i:6;s:8:\"Surabaya\";}i:4;a:2:{i:9;s:21:\"Microsoft Office Home\";i:10;s:1:\"5\";}}', '2016-04-20 19:32:12', '2016-04-20 19:32:12', null), ('5', '1', '3', 'a:2:{i:3;a:5:{i:4;s:13:\"Jessica Jones\";i:8;s:21:\"jesse.jones@gmail.com\";i:7;s:11:\"08123215677\";i:5;s:7:\"Araya 2\";i:6;s:7:\"Jakarta\";}i:4;a:2:{i:9;s:12:\"Software ERP\";i:10;s:1:\"5\";}}', '2016-04-20 19:34:40', '2016-04-20 19:34:40', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpForms`
-- ----------------------------
DROP TABLE IF EXISTS `tpForms`;
CREATE TABLE `tpForms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `havePage` tinyint(1) DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `tpforms_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tpCompanies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpForms`
-- ----------------------------
BEGIN;
INSERT INTO `tpForms` VALUES ('1', '1', 'Survei Pelanggan', '', '1', 'form-1', '1', '2016-04-20 16:23:40', '2016-04-20 16:23:40', null), ('2', '1', 'Pre-Order Form', '', '1', 'form-2', '0', '2016-04-20 16:48:22', '2016-04-20 16:48:22', null), ('3', '1', 'Product Quality', 'Customer products rating', '1', 'product-quality', '1', '2016-04-20 16:46:25', '2016-04-20 16:44:23', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpLocationLogs`
-- ----------------------------
DROP TABLE IF EXISTS `tpLocationLogs`;
CREATE TABLE `tpLocationLogs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `marketingId` bigint(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marketingId` (`marketingId`),
  CONSTRAINT `tplocationlogs_ibfk_1` FOREIGN KEY (`marketingId`) REFERENCES `tpMarketings` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `tpMarketingProfiles`
-- ----------------------------
DROP TABLE IF EXISTS `tpMarketingProfiles`;
CREATE TABLE `tpMarketingProfiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `marketingId` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marketingId` (`marketingId`),
  CONSTRAINT `tpmarketingprofiles_ibfk_1` FOREIGN KEY (`marketingId`) REFERENCES `tpMarketings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpMarketingProfiles`
-- ----------------------------
BEGIN;
INSERT INTO `tpMarketingProfiles` VALUES ('1', '1', 'Yusuf', '', '', '2016-03-01 16:03:34', '2016-03-01 16:03:34', null), ('2', '2', 'Antonius Henry Setiawan', 'Ploso Timur 1/110', '+6281310310927', '2016-03-01 16:12:56', '2016-03-01 16:12:56', null), ('3', '3', 'Frozenkyo', '', '', '2016-04-30 15:03:31', '2016-04-30 15:03:31', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpMarketings`
-- ----------------------------
DROP TABLE IF EXISTS `tpMarketings`;
CREATE TABLE `tpMarketings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `taskId` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pwdLen` int(3) DEFAULT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `tpmarketings_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tpCompanies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpMarketings`
-- ----------------------------
BEGIN;
INSERT INTO `tpMarketings` VALUES ('1', '1', '0', 'achyusuf16@gmail.com', 'BD6CBCC41DF0DC711AEF6D73A6A3D8D6751E9154', '16', '2016-03-01 16:03:34', '2016-03-01 16:03:34', null), ('2', '1', '0', 'antonius.henry.setiawan@gmail.com', 'D648A0DF6BBD90860DC9B07B07B06D9686AFCAA8', '8', '2016-03-01 22:19:28', '2016-03-01 16:12:56', null), ('3', '1', '0', 'antonius.henry.89@gmail.com', 'BDED957A36A131A8173397D6A1B492D820ACF7DB', '16', '2016-03-01 16:23:59', '2016-03-01 16:23:59', null);
COMMIT;

-- ----------------------------
--  Table structure for `tpSessions`
-- ----------------------------
DROP TABLE IF EXISTS `tpSessions`;
CREATE TABLE `tpSessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `tpSessions`
-- ----------------------------
BEGIN;
INSERT INTO `tpSessions` VALUES ('012add8c7381872765cd35e7acdfc78ecf2b5b72', '127.0.0.1', '1462171299', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323137313239393b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('17922f4f88286b182a9441c4ebf1c07989564ff9', '127.0.0.1', '1462260457', 0x747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323236303432393b7d7d5f5f63695f6c6173745f726567656e65726174657c693a313436323236303432393b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d), ('23c9243b352dd148c06bc8b628b2f6afb110ae54', '127.0.0.1', '1462162628', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136323334383b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('33bf9f6e53df67dc98f33a2448ad972bfaa6ef5c', '127.0.0.1', '1462168406', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136383132373b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('4e2efd394d982b97ef3fc4495cae6452b41c3fa3', '127.0.0.1', '1462160082', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323135393830303b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('564755852a107d0442858c59ed571ce920e13ac1', '127.0.0.1', '1462172797', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323137323536393b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('56afb7d88d08023f7502334b8a549657e081e327', '127.0.0.1', '1462324531', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323332343532383b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d), ('6385e1825ff75663df8e7c782c5d9f3a9d6b3269', '::1', '1462963717', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323936333731363b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d), ('65b4887329539e119a1df01e24658f3bc88b5e08', '127.0.0.1', '1462159800', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323135353938303b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('6bc229255fbc4c4af9c941e3a52896d7b61f9664', '127.0.0.1', '1462464694', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323436343639333b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d), ('841057aa60266c811de0a8be19e18845a7ef5f7f', '::1', '1462963503', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323936333530303b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d), ('8480e1e7387f62f1ba4bb1cc8c5c1a5b6efb8031', '127.0.0.1', '1462160173', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136303130323b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('924eebc2dfbef666173f11ba9bd5bbe986b99407', '127.0.0.1', '1462168117', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136373832363b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('aca8fda2ee324001c0433e5776a9f967760e7de7', '127.0.0.1', '1462162305', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136323031343b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('ad4949ff64c43ff808e9a099c972b4af2439dca4', '127.0.0.1', '1462173386', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323137333230303b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('c78e055475fdaa575f259737859c9d363ba2ce2a', '127.0.0.1', '1462173186', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323137323838383b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('d1ba9e80c127da3fa8f35518034bf8f8314bbc5c', '127.0.0.1', '1462161117', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136313035323b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('f0b1f62137eb56993d30ee7efaef8c9f0560975e', '127.0.0.1', '1462168449', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323136383433353b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d747261636b706c75737c613a313a7b733a343a2275736572223b613a333a7b733a323a226964223b733a313a2231223b733a393a22636f6d70616e794964223b733a313a2231223b733a31303a226c6f67696e5f74696d65223b693a313436323135393830303b7d7d), ('f75fa7fece174519299441b39a8f4e0d592920d8', '127.0.0.1', '1462238548', 0x5f5f63695f6c6173745f726567656e65726174657c693a313436323233383534373b706167654a537c613a313a7b733a363a22706167654a53223b613a313a7b733a31303a226a617661736372697074223b733a303a22223b7d7d);
COMMIT;

-- ----------------------------
--  Table structure for `tpTaskList`
-- ----------------------------
DROP TABLE IF EXISTS `tpTaskList`;
CREATE TABLE `tpTaskList` (
  `taskId` bigint(20) NOT NULL DEFAULT '0',
  `customerId` bigint(20) NOT NULL DEFAULT '0',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`taskId`,`customerId`),
  KEY `customerId` (`customerId`),
  CONSTRAINT `tptasklist_ibfk_1` FOREIGN KEY (`taskId`) REFERENCES `tpTasks` (`id`),
  CONSTRAINT `tptasklist_ibfk_2` FOREIGN KEY (`customerId`) REFERENCES `tpCustomers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `tpTasks`
-- ----------------------------
DROP TABLE IF EXISTS `tpTasks`;
CREATE TABLE `tpTasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `marketingId` bigint(20) DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `taskRepeat` int(11) DEFAULT '0' COMMENT '0: No Repeat; 1: Every Day; 7: Every Week; 14: Every 2 Weeks; 30: Every Month;',
  `taskStatus` int(11) DEFAULT '2' COMMENT '0: Expired/Inactive; 1: Active; 2: On Schedule;',
  `updatedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marketingId` (`marketingId`),
  CONSTRAINT `tptasks_ibfk_1` FOREIGN KEY (`marketingId`) REFERENCES `tpMarketings` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Triggers structure for table tpAdmins
-- ----------------------------
DROP TRIGGER IF EXISTS `createAdminProfile`;
delimiter ;;
CREATE TRIGGER `createAdminProfile` AFTER INSERT ON `tpAdmins` FOR EACH ROW BEGIN
		INSERT INTO tpAdminProfiles(adminId, name, address, phone, updatedAt, createdAt)
		VALUES(NEW.id, "", "", "", NEW.updatedAt, NEW.createdAt);
	END
 ;;
delimiter ;

delimiter ;;
-- ----------------------------
--  Triggers structure for table tpMarketings
-- ----------------------------
 ;;
delimiter ;
DROP TRIGGER IF EXISTS `createMarketingProfile`;
delimiter ;;
CREATE TRIGGER `createMarketingProfile` AFTER INSERT ON `tpMarketings` FOR EACH ROW BEGIN
		INSERT INTO tpMarketingProfiles(marketingId, name, address, phone, updatedAt, createdAt)
		VALUES(NEW.id, "", "", "", NEW.updatedAt, NEW.createdAt);
	END
 ;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
