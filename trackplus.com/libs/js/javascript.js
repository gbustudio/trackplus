var trackplus = {};


//test  scroll load
/*$(window).scroll(function (event) {
    scroll = $(window).scrollTop();
    offset = $('.ajax-call-container').offset();
    if(typeof offset != "undefined"){
        AjaxLoadContainerTop = offset.top;
        var AjaxLoadContainerHeight = $('.ajax-call-container').height() - 200;
        marginTop = scroll - AjaxLoadContainerTop;
        if((marginTop > 0)&&(marginTop < AjaxLoadContainerHeight)){
        $('#ajax_call_load').css('margin-top', marginTop);
        $('#ajax_call_load').css({'height':'calc(100% - '+marginTop+'px)'});
        }
    }
});*/
/*$(document).ready(function() {
    var table = $('#example').DataTable();
     
    $('#example tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        alert( 'You clicked on '+data[0]+'\'s row' );
    } );
} );*/



trackplus.request = {
	/*ajax_request: function(route, postData) {
		// Clear container first
		$('#response_container').html('');

		$.ajax({
            url: baseURL+'request/'+route,
            dataType: 'JSON',
            type: 'POST',
            data: postData,
            success: function(json){
                console.log(json);
                if (json.status == 'OK') {
                	if (typeof json.js != undefined) {
	                	eval(json.js);
	                }
                } else {
                	if (typeof json.error != undefined) {
	                	//console.log(json.errors);
                        trackplus.response.showError(json.errors);
	                }
                }
            },
            error: function(response) {
            	console.log(response);
            }
        });
	},*/

    //test loading aras

    ajax_request: function(route, postData) {
        // Clear container first
        $('#response_container').html('');
        var loadingGIF = $('.ajax-call-container');

        $.ajax({
            url: baseURL+'request/'+route,
            dataType: 'JSON',
            type: 'POST',
            data: postData,
            beforeSend: function() {
                loadingGIF.removeClass('hidden');
                //wwf.request.getOffsetValueAJAXload();
                console.log('before send!!');
            },
            success: function(json){
                console.log(json);
                if (json.status == 'OK') {
                    if (typeof json.js != 'undefined') {
                        eval(json.js);
                    }
                    if (typeof json.success != 'undefined') {
                        trackplus.response.showSuccess(json.success);
                    }
                } else {
                    if (typeof json.errors != 'undefined') {
                        trackplus.response.showError(json.errors);
                    }
                }
                loadingGIF.addClass('hidden');
            },
            error: function(response) {
                console.log(response);
            }
        });
    },

};

trackplus.modal = {

};

trackplus.profile = {
    save: function(id)
    {
        var data = {
            "action": "save",
            "id"    : id
        };
        var post = $.param(data)+"&"+$("#profile").serialize();
        trackplus.request.ajax_request("profile", post);
    },
    chPass: function(id)
    {
        var data = {
            "action": "chpass",
            "id"    : id
        };
        var post = $.param(data)+"&"+$("#profile").serialize();
        trackplus.request.ajax_request("profile", post);
    }
};

trackplus.admin = {
    add: function()
    {
        if( (!trackplus.utils.isValidEmail($("#adminEmail").val())) ){
            msg = {
                "email" : "Penulisan email tidak valid"
            }
            trackplus.response.showError(msg);
            return;
        }

        // if($("#addPassword").val() != $("#confirmPassword").val()){
        //     msg = {
        //         "email" : "Konfirmasi password tidak sama!"
        //     }
        //     trackplus.response.showError(msg);
        //     return;
        // }

        action = 'action=add';
        postData = $('#add_admin').serialize();
        postData = action + "&" + postData;
        trackplus.request.ajax_request("admin", postData);
    },

    edit: function(id)
    {
        if(!trackplus.utils.isNumber($('#adminPhone').val()) && ($('#adminPhone').val() != "")){
            phoneErr = {
                'phone' : 'Penulisan nomor telepon tidak valid'
            }
            trackplus.response.showError(phoneErr);
        } else {
            action = 'action=edit';
            postData = action + "&" + $('#add_admin').serialize()+"&id="+id;
            console.log(postData);
            trackplus.request.ajax_request("admin", postData);
        }
    },

    delete: function(id, element)
    {
        element.parents('tr').addClass('selected');
        action = 'action=delete';
        postData = 'adminId='+id;
        postData = action + '&' + postData;
        console.log(postData);
        trackplus.request.ajax_request('admin', postData);
    },
};


trackplus.customer = {
    add: function()
    {
        action = 'action=add';
        postData = $('#customer').serialize();
        postData = action + "&" + postData;
        trackplus.request.ajax_request("customer", postData);
    },
    edit: function(id)
    {
        action = 'action=edit';
        postData = action + "&" + $('#customer').serialize()+"&id="+id;
        trackplus.request.ajax_request("customer", postData);
    },

    delete: function(id, element)
    {
        element.parents('tr').addClass('selected');
        action = 'action=delete';
        postData = 'customerId='+id;
        postData = action + '&' + postData;
        trackplus.request.ajax_request('customer', postData);
    }
}

trackplus.marketing = {
    add_marketing: function()
    {
        if( (!trackplus.utils.isValidEmail($("#addEmail").val())) ){
            msg = {
                "email" : "Penulisan email tidak valid"
            }
            trackplus.response.showError(msg);
            return;
        }

        if($("#newPassword").val() != $("#confirmPassword").val()){
            msg = {
                "email" : "Konfirmasi password tidak sama!"
            }
            trackplus.response.showError(msg);
            return;
        }

        action = 'action=add_new';
        postData = $('#add_marketing').serialize();
        postData = action + "&" + postData;
        trackplus.request.ajax_request("marketing", postData);
    },

    delete_marketing: function(id, element)
    {
        element.parents('tr').addClass('selected');
        action = 'action=delete';
        postData = 'marketingId='+id;
        postData = action + '&' + postData;
        console.log(postData);
        trackplus.request.ajax_request('marketing', postData);
    },

    edit_marketing: function(id){
        if(!trackplus.utils.isNumber($('#addMarketingPhone').val()) && ($('#addMarketingPhone').val() != "")){
            phoneErr = {
                'phone' : 'Penulisan nomor telepon tidak valid'
            }
            trackplus.response.showError(phoneErr);
        } else {
            action = 'action=edit';
            postData = action + "&" + $('#add_marketing').serialize()+"&id="+id;
            console.log(postData);
            trackplus.request.ajax_request("marketing", postData);
        }
    }
};

trackplus.announcements = {
    save: function(id) {
        var post = {
                action: "add",
                title: $("#announcementTitle").val(),
                content: CKEDITOR.instances.announcementEditor.getData()
            };

        if (typeof(id) != "undefined") {
            post.action = "edit";
            post.id = id;
        }

        trackplus.request.ajax_request("announcement", $.param(post) + "&" + $("#projectContainer input").serialize());
    },
    delete: function(id, element) {
        element.parents('tr').addClass('selected');
        var post = {
            action: "delete",
            id: id
        };

        trackplus.request.ajax_request("announcement", $.param(post));
    },
};

trackplus.form = {
    add_form: function(){
        $('#response_container').empty();
        if($('#projectName').val() == ""){
            namaErr = {
                'nama' : 'nama dibutuhkan!'
            }
            trackplus.response.showError(namaErr);
        } else {
            action = 'action=add_new';
            post = $('#add_project').serialize();
            post = action + '&' + post;
            trackplus.request.ajax_request('form', post);
        }
    },
    edit_form: function(id){
        var unchecked = '';
        action = 'action=edit';
        postData = action + unchecked + "&" + $('#add_project').serialize()+"&id="+id;
        trackplus.request.ajax_request("form", postData);
    },
    /*addNewPage: function(){
        page = $("#addPage").val();
        $("#addPage").val("");
        $("#addPage").focus();
        append ="\
        <div class='pageName border' style='margin:10px; padding:8px; cursor:pointer' onclick='trackplus.project.addBuilFormPanel()'>\
            "+page+"\
            <a class='no-underline remove-page pull-right' onclick='$(this).parents(\".pageName\").remove(); trackplus.project.countPage()'><i class='fa fa-trash'></i></a>\
        </div>\
        ";
        $("#pageContainer").append(append);
        console.log($("#addPage").val());
        trackplus.form.countPage();
    },*/
    /*countPage: function(){
        console.log($('.pageName').length);
        count = $('.pageName').length.toString();
        $('#pageCount').html( count <= 1 ? count + ' Page Added' : count + ' Pages Added');
    },*/
    publish: function(id) {
        var post = {
            "id"    : id,
            "action": "publish"
        };
        trackplus.request.ajax_request("form", post);
    }
};

trackplus.form.builder = {
    addPage: function(id) {
        if ($("#form_page_input").find('input[type="text"]').length <= 0) {
            var post = {
                "id"    : id,
                "action": "add"
            };
            trackplus.request.ajax_request("page", post);
        } else {
            console.log("Focus Input");
            $("#form_page_input input").focus();
        }
    },
    reorderPage: function() {
        $("#reorderPage").addClass("hidden");
        $("#saveReorderPage").removeClass("hidden");
        $("#pageContainer div.action-btn").addClass("hidden");
        $("#pageContainer div.page-edit-information").addClass('hidden');
        $("#pageContainer div.page-information").removeClass('hidden');

        $("#pageContainer").sortable({
            cursor: "move",
            items: "div.row"
        });
    },
    saveReorderPage: function(id) {
        var post = {
            "action": "reorder",
            "id"    : id
        };

        post = $.param(post) + "&" + $.param($("#pageContainer input"));
        trackplus.request.ajax_request("page", post);
    },
    showOptions: function(type) {
        var post = {
            "action": "reorder",
            "id"    : id,
        };
    },
};

trackplus.authentication = {
    test: function() {
        console.log("test");
    },
	login: function(button) {
		action = "action=login";

		console.log($(button).parents("form").serialize());
		postData = $(button).parents("form").serialize();

		postData = action + "&" + postData;
		console.log(baseURL+"request/user");
		trackplus.request.ajax_request("user", postData);
	},
	logout: function() {
		window.location.replace(baseURL+"authentication/logout");
	}
};

trackplus.sidebar = {
    toggleSidebar: function(element){
        $( "#trackplus-sidebar" ).toggle( "slide", {direction:'left'}, 800 );
        $("#trackplus-content").toggleClass("col-md-offset-2", 800 );
        $("#trackplus-content").toggleClass("col-md-10");
    },
};

trackplus.response = {
    showError: function(data){
        errCounter = 1;
        err = '<div id="error-response" class="alert alert-danger" role="alert">';
        err += '<ul>';
        $.each(data, function(key, val) {
        	err += '<li>'+val+'</li>';
        });
        err += '</ul>';
        err += '</div>';
        $('#response_container').html(err);
    },
    showSuccess: function(data) {
        msgCounter = 1;
        msg = '<div id="success-response" class="alert alert-success" role="alert">';
        msg += '<ul>';
        $.each(data, function(key, val) {
            msg += '<li>'+val+'</li>';
        });
        msg += '</ul>';
        msg += '</div>';
        $('#response_container').html(msg);
    },
};

trackplus.utils = {
    isValidEmail: function(emailAddress){
        var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(emailAddress);
    },

    isNumber: function(phoneNumber){
        var regex = /^(?=.*\d)[\d ]+$/;
        return regex.test(phoneNumber);
    }
};