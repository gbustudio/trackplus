Documentation:

hosts file:
127.0.0.1	local.trackplus

virtual hosts:
<VirtualHost *:80>
	DocumentRoot D:/www/trackplus/trackplus.com
	ServerName local.trackplus
	<Directory D:/www/trackplus/>
		Options All
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>