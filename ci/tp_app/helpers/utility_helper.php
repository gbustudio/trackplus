<?php defined('BASEPATH') OR exit('No direct script access allowed');

	function lib_url($path = null)
	{
		return base_url() . 'libs/' . $path;
	}

	function upload_url($path = null)
	{
		return base_url() . 'uploads/' . $path;
	}

	function assets_url($path = null)
	{
		return base_url() . 'assets/' . $path;
	}

	function getTimestamp($time)
	{
		return date('Y-m-d H:i:s', $time);
	}

	function getSlugFromString($string)
	{
		$slug = preg_replace('/[^a-zA-Z0-9 ]+/', '', $string);
		$slug = str_replace(' ', '-', $slug);
		return strtolower($slug);
	}

	function renderHTMLString($html) {
		$string = trim(preg_replace('/\s+/', ' ', $html));
		return $string;
	}

	function propercase($string) {
		return ucwords(strtolower($string));
	}