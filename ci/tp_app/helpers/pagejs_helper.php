<?php defined('BASEPATH') OR exit('No direct script access allowed');

	function addDomreadyJS($js)
	{
		$_ci =& get_instance();
		if(!is_string($js)){
			log_message('error', 'Attempting to eval non-string value JS.');
			return;
		}
		$pageJSObj = $_ci->session->userdata('pageJS');
		if(isset($pageJSObj['pageJS']['javascript'])){
			$pageJSObj['pageJS']['javascript'] .= $js;
		}else{
			$pageJSObj['pageJS']['javascript'] = $js;
		}
		$_ci->session->set_userdata('pageJS', $pageJSObj);
		return;
	}

	function printDomreadyJS($echo=true)
	{
		$_ci =& get_instance();
		$return = false;
		$pageJSObj = $_ci->session->userdata('pageJS');
		if(isset($pageJSObj['pageJS']['javascript'])){
			if($echo === true){
				echo $pageJSObj['pageJS']['javascript'];
				$return = true;
			}else{
				$return = $pageJSObj['pageJS']['javascript'];
			}
			$pageJSObj['pageJS']['javascript'] = '';
		}
		$_ci->session->set_userdata('pageJS', $pageJSObj);
		return $return;
	}