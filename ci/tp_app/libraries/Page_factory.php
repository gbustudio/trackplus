<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();
	}

	public function get($formId) {
		$query = $this->_ci->db->query('SELECT * FROM '.TBL_FORM_PAGES.' WHERE formId = '.$this->_ci->db->escape($formId).' AND deletedAt IS NULL ORDER BY sequence ASC');

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	public function getOne($pageId) {
		$query = $this->_ci->db->query('SELECT * FROM '.TBL_FORM_PAGES.' WHERE id = '.$this->_ci->db->escape($pageId).' AND deletedAt IS NULL');

		if ($query->num_rows() == 1) {
			return $query->row();
		}
		return false;
	}

	public function save($page) {
		$result = $this->_ci->db->insert(TBL_FORM_PAGES, $page);
		if ($result) {
			return $this->_ci->db->insert_id();
		}

		return false;
	}

	public function update($pageId, $page) {
		$result = $this->_ci->db->update(TBL_FORM_PAGES, $page, array('id' => $pageId));
		return $result;
	}

	public function delete($pageId, $page) {
		$result = $this->_ci->db->update(TBL_FORM_PAGES, $page, array('id' => $pageId));
		if ($result) {
			return true;
		}

		return false;
	}

	public function getMaxSequence($formId) {
		$result = 1;
		$query = $this->_ci->db->query('SELECT MAX(sequence) latest_sequence FROM '.TBL_FORM_PAGES.' WHERE formId = '.$this->_ci->db->escape($formId).' AND deletedAt IS NULL');

		if ($query->num_rows() > 0) {
			$result = intval($query->row()->latest_sequence) + 1;
		}
		return $result;
	}

	public function haveNext($formId, $sequence) {
		$queryString = '
			SELECT *
			FROM '.TBL_FORM_PAGES.'
			WHERE formId = '.$this->_ci->db->escape($formId).' AND
				  sequence > '.$this->_ci->db->escape($sequence).' AND
				  deletedAt IS NULL
		';

		$query = $this->_ci->db->query($queryString);

		if ($query->num_rows() > 0) {
			return true;
		}
		return false;
	}

	public function havePrev($formId, $sequence) {
		$queryString = '
			SELECT *
			FROM '.TBL_FORM_PAGES.'
			WHERE formId = '.$this->_ci->db->escape($formId).' AND
				  sequence < '.$this->_ci->db->escape($sequence).' AND
				  deletedAt IS NULL
		';

		$query = $this->_ci->db->query($queryString);

		if ($query->num_rows() > 0) {
			return true;
		}
		return false;
	}

	public function isDuplicatedName($formId, $name, $pageId = 0) {
		$queryString = '
			SELECT *
			FROM '.TBL_FORM_PAGES.'
			WHERE formId = '.$this->_ci->db->escape($formId).' AND
				  lower(name) = '.$this->_ci->db->escape(strtolower($name)).' AND
				  deletedAt IS NULL
		';

		if ($pageId > 0) {
			$queryString .= 'AND id <> '.$this->_ci->db->escape($pageId);
		}

		$query = $this->_ci->db->query($queryString);

		if ($query->num_rows() > 0) {
			return true;
		}

		return false;
	}

}