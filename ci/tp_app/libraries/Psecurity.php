<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Psecurity {
	private $_ci;

	function __construct()
	{
		$this->_ci =& get_instance();
	}

	public function encryptPassword($md5Password, $pwdRawLen)
	{
		$enp = md5($md5Password);
        for ($i=0; $i<500; $i++) {
            $enp = sha1($enp.'_trackplussalt'.$pwdRawLen);
        }
        return strtoupper($enp);
	}

	public function isUserLoggedIn() {
		$sess_security = $this->_ci->session->userdata('trackplus');
		if (!is_null($sess_security) && isset($sess_security['user'])) {
			return true;
		}
		return false;
	}

	public function getUserLoggedIn($tag = 'id')
	{
		$sess_security = $this->_ci->session->userdata('trackplus');
		if (!is_null($sess_security) && isset($sess_security['user'])) {
			switch ($tag) {
				case 'company':
					return $sess_security['user']['companyId'];
				default:
					return $sess_security['user']['id'];
			}
		}
		return false;
	}

	public function getUserLoggedInCompany()
	{
		$sess_security = $this->_ci->session->userdata('trackplus');
		if (!is_null($sess_security) && isset($sess_security['user'])) {
			return $sess_security['user']['companyId'];
		}
		return false;
	}

	public function authenticateAdmin($email, $password)
	{
		$md5Password = md5($password);
		$passwordLength = strlen($password);
		$encPassword = $this->encryptPassword($md5Password, $passwordLength);

		$query = $this->_ci->db->query('SELECT id, companyId
										FROM '.TBL_ADMINS.'
										WHERE lower(email) = '.$this->_ci->db->escape(strtolower($email)).' AND
											  password = '.$this->_ci->db->escape($encPassword).' AND
											  pwdLen = '.$this->_ci->db->escape($passwordLength).'
				 ');

		if ($query->num_rows() == 1) {
			$row = $query->row();

			$this->_ci->session->unset_userdata('trackplus');

			$sess['user']['id'] = $row->id;
			$sess['user']['companyId'] = $row->companyId;
			$sess['user']['login_time'] = time();
			$this->_ci->session->set_userdata('trackplus', $sess);

			$this->_ci->db->query('INSERT INTO '.TBL_ADMIN_ACCESS_LOG.' (adminId, userAgent, ipAddress)
								   VALUES (
										'.$this->_ci->db->escape($row->id).',
										'.$this->_ci->db->escape($this->_ci->input->user_agent()).',
										'.$this->_ci->db->escape($this->_ci->input->ip_address()).'
								   	);
			');

			return true;
		}

		return false;
	}

	public function authenticateMarketing($email, $password)
	{
		$md5Password = md5($password);
		$passwordLength = strlen($password);
		$encPassword = $this->encryptPassword($md5Password, $passwordLength);

		$query = $this->_ci->db->query('SELECT id, companyId
										FROM '.TBL_MARKETINGS.'
										WHERE lower(email) = '.$this->_ci->db->escape(strtolower($email)).' AND
											  password = '.$this->_ci->db->escape($encPassword).' AND
											  pwdLen = '.$this->_ci->db->escape($passwordLength).'
				 ');

		if ($query->num_rows() == 1) {
			$row = $query->row();

			/*$this->_ci->session->unset_userdata('trackplus');

			$sess['user']['id'] = $row->id;
			$sess['user']['companyId'] = $row->companyId;
			$sess['user']['login_time'] = time();
			$this->_ci->session->set_userdata('trackplus', $sess);*/

			/*$this->_ci->db->query('INSERT INTO '.TBL_ADMIN_ACCESS_LOG.' (adminId, userAgent, ipAddress)
								   VALUES (
										'.$this->_ci->db->escape($row->id).',
										'.$this->_ci->db->escape($this->_ci->input->user_agent()).',
										'.$this->_ci->db->escape($this->_ci->input->ip_address()).'
								   	);
			');*/

			return true;
		}

		return false;
	}

	public function checkCurrentPassword($id, $password)
	{
		$pwdLen = strlen($password);
		$enp = $this->encryptPassword(md5($password), $pwdLen);
		$query = $this->_ci->db->query('SELECT id
										FROM '.TBL_ADMINS.'
										WHERE id = '.$this->_ci->db->escape($id).' AND
											  password = '.$this->_ci->db->escape($enp).' AND
											  pwdLen = '.$this->_ci->db->escape($pwdLen).'
				 ');
		if ($query->num_rows() == 1) {
			return true;
		}
		return false;
	}

	public function logout()
	{
		$this->_ci->session->unset_userdata('trackplus');
	}

}