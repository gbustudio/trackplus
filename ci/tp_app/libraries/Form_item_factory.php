<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Form_item_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();
	}

	public function get($formId, $pageId) {
		$query = $this->_ci->db->query('
				SELECT *
				FROM '.TBL_FORM_ITEMS.'
				WHERE formId = '.$this->_ci->db->escape($formId).' AND
					  pageId = '.$this->_ci->db->escape($pageId).' AND
					  deletedAt IS NULL
				ORDER BY sequence ASC
			');

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	public function getOne($itemId) {
		$query = $this->_ci->db->query('
				SELECT *
				FROM '.TBL_FORM_ITEMS.'
				WHERE id = '.$this->_ci->db->escape($itemId).' AND
					  deletedAt IS NULL
			');

		if ($query->num_rows() == 1) {
			return $query->row();
		}
		return false;
	}

	public function getAll($formId, $havePage=0) {
		if ($havePage <= 0) {
			$query = $this->_ci->db->query('
				SELECT *
				FROM '.TBL_FORM_ITEMS.'
				WHERE formId = '.$this->_ci->db->escape($formId).' AND
					  deletedAt IS NULL
				ORDER BY sequence ASC
			');
		} else {
			$query = $this->_ci->db->query('
				SELECT F.* FROM '.TBL_FORM_ITEMS.' F, '.TBL_FORM_PAGES.' P
				WHERE F.formId = '.$this->_ci->db->escape($formId).' AND
					  P.id = F.pageId AND
					  P.deletedAt IS NULL AND
					  F.deletedAt IS NULL
				ORDER BY P.sequence ASC, F.sequence ASC
			');
		}

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	public function save($item) {
		$result = $this->_ci->db->insert(TBL_FORM_ITEMS, $item);
		if ($result) {
			return $this->_ci->db->insert_id();
		}

		return false;
	}

	public function update($itemId, $item) {
		$result = $this->_ci->db->update(TBL_FORM_ITEMS, $item, array('id' => $itemId));
		return $result;
	}

	public function delete($itemId, $softDelete = true) {
		if ($softDelete) {
			$result = $this->_ci->db->update(TBL_FORM_ITEMS, $page, array('id' => $itemId));
			if ($result) {
				return true;
			}
		} else {
			$result = $this->_ci->db->delete(TBL_FORM_ITEMS, array('id' => $itemId));
			if ($result) {
				return true;
			}
		}

		return false;
	}

	public function getMaxSequence($formId, $pageId) {
		$result = 1;
		$query = $this->_ci->db->query('
				SELECT MAX(sequence) latest_sequence
				FROM '.TBL_FORM_ITEMS.'
				WHERE formId = '.$this->_ci->db->escape($formId).' AND
					  pageId = '.$this->_ci->db->escape($pageId).' AND
					  deletedAt IS NULL
			');

		if ($query->num_rows() > 0) {
			$result = intval($query->row()->latest_sequence) + 1;
		}
		return $result;
	}

	public function isDuplicatedName($formId, $pageId, $name, $itemId = 0) {
		$queryString = '
			SELECT *
			FROM '.TBL_FORM_ITEMS.'
			WHERE formId = '.$this->_ci->db->escape($formId).' AND
				  pageId = '.$this->_ci->db->escape($pageId).' AND
				  lower(name) = '.$this->_ci->db->escape(strtolower($name)).' AND
				  deletedAt IS NULL
		';

		if ($itemId > 0) {
			$queryString .= 'AND id <> '.$this->_ci->db->escape($itemId);
		}

		$query = $this->_ci->db->query($queryString);

		if ($query->num_rows() > 0) {
			return true;
		}

		return false;
	}

	public function getSubmissions($formId) {
		$query = $this->_ci->db->query('
				SELECT IF(MP.name = "", M.email, MP.`name`) Marketing, S.createdAt Date, S.submission Submission
				FROM '.TBL_FORM_SUBMISSIONS.' S, '.TBL_MARKETINGS.' M, '.TBL_MARKETING_PROFILES.' MP
				WHERE S.marketingId = M.id AND
					  M.id = MP.marketingId AND
					  S.formId = '.$this->_ci->db->escape($formId).' AND
					  S.deletedAt is NULL
				ORDER BY S.createdAt
			');
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

}