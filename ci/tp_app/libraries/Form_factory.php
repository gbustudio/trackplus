<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Form_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();
	}

	public function get($companyId) {
		$query = $this->_ci->db->query('SELECT * FROM '.TBL_FORMS.' WHERE companyId = '.$this->_ci->db->escape($companyId).' AND deletedAt IS NULL');

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	public function getOne($projectId) {
		$query = $this->_ci->db->query('SELECT * FROM '.TBL_FORMS.' WHERE id = '.$this->_ci->db->escape($projectId).' AND deletedAt IS NULL');

		if ($query->num_rows() == 1) {
			return $query->row();
		}
		return false;
	}

	/*public function getAvailableMarketing($projectId) {
		$query = $this->_ci->db->query('
				SELECT M.id, IF(MP.name = "", M.email, MP.name) AS name, M.projectId
				FROM '.TBL_MARKETINGS.' M, '.TBL_MARKETING_PROFILES.' MP
				WHERE M.deletedAt IS NULL AND
					  M.id = MP.marketingId AND
					  M.companyId = '.$this->_ci->db->escape($this->_ci->psecurity->getUserLoggedIn('company')).' AND
					  (M.projectId = 0 OR M.projectId = '.$projectId.')
			');

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}*/

	/*public function getAvailableMarketingsId($formId) {
		$query = $this->_ci->db->query('
				SELECT id
				FROM '.TBL_MARKETINGS.'
				WHERE deletedAt IS NULL AND
					  companyId = '.$this->_ci->db->escape($this->_ci->psecurity->getUserLoggedIn('company')).' AND
					  (formId = 0 OR formId = '.$this->_ci->db->escape($formId).')
			');

		if ($query->num_rows() > 0) {
			$result = $query->result();
			return array_map(function($arr) { return $arr->id; }, $result);
		}
		return false;
	}*/

}