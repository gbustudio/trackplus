<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pagebuilder {
	private $_ci;

	function __construct()
	{
		$this->_ci =& get_instance();
		$this->_ci->load->library(array('admin_factory'));
	}

	private function getMenus()
	{
		$menus = array(
				'dashboard' => array(
						'icon'	=> 'fa fa-tachometer',
						'label' => 'Dashboard',
						'url' 	=> site_url('dashboard'),
					),
				'admin' => array(
						'icon'  => 'fa fa-user',
						'label' => 'Admins',
						'url'   => site_url('admin'),
					),
				'announcement' => array(
						'icon'  => 'fa fa-bullhorn',
						'label' => 'Announcements',
						'url'   => site_url('announcement'),
					),
				'customer' => array(
						'icon'  => 'fa fa-building-o',
						'label' => 'Customers',
						'url'   => site_url('customer'),
					),
				'task' => array(
						'icon'  => 'fa fa-tasks',
						'label' => 'Tasks',
						'url'   => site_url('task'),
					),
				'marketing' => array(
						'icon'  => 'fa fa-users',
						'label' => 'Marketings',
						'url'   => site_url('marketing'),
					),
				'form' => array(
						'icon'  => 'fa fa-briefcase',
						'label' => 'Forms',
						'url'   => site_url('form'),
					),
				'log' => array(
						'icon'  => 'fa fa-history',
						'label' => 'Logs',
						'url'   => site_url('log'),
					),
			);

		return $menus;
	}

	public function renderPage($view, $title='', $data=array(), $useMenu=false)
	{
		$pageTitle = 'Trackplus';
		if (strlen($pageTitle) > 0) {
			$pageTitle .= ' - '.$title;
		}

		$page = $this->_ci->load->view('components/header', array('title' => $pageTitle), true);

		if ($useMenu) {
			$html = $this->_ci->load->view($view, $data, true);
			$screenName = $this->_ci->admin_factory->getScreenName($this->_ci->psecurity->getUserLoggedIn());
			$menus = $this->getMenus();

			$page .= $this->_ci->load->view('components/navbar', compact('screenName'), true);
			$page .= $this->_ci->load->view('components/sidebar', compact('menus', 'html', 'title'), true);
		} else {
			$page .= $this->_ci->load->view($view, $data, true);
		}

		$page .= $this->_ci->load->view('components/footer', '', true);
		return $page;
	}

}