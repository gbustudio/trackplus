<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Form_builder {
	private $_ci;

	function __construct()
	{
		$this->_ci =& get_instance();
		$this->_ci->load->library(array('form_factory','page_factory','form_item_factory'));
	}

	public function renderPageList($formId) {
		$pages = $this->_ci->page_factory->get($formId);
		$html = '';

		if ($pages != false) {
			foreach ($pages as $page) {
				$html .= $this->renderPageRow($formId, $page->id);
			}
		}

		return $html;
	}

	public function renderPageRow($formId, $pageId) {
		$page = $this->_ci->page_factory->getOne($pageId);
		$html = '';

		if ($page != false) {
			$html = '
				<div id="page_'.$pageId.'_row" class="row page-row">
					<div id="page_'.$pageId.'_information" class="page-information">
						<input type="hidden" name="sequence[]" value="'.$pageId.'" />
						<!--
						<div class="move-btn col-md-1">
							<span><i class="fa fa-arrows"></i></span>
						</div>
						-->
						<div id="page_'.$pageId.'_name" class="col-md-6">
							<span class="page-name">'.$page->name.'</span>
						</div>
						<div class="action-btn col-md-6 text-right">
							<a id="page_'.$pageId.'_delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" class="pull-right">
								<i class="fa fa-trash"></i>
							</a>
							<a id="page_'.$pageId.'_edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit" class="pull-right">
								<i class="fa fa-pencil"></i>
							</a>
						</div>
						<div class="col-md-12" style="margin-top:10px; margin-bottom:10px; border-bottom:1px solid #BFBFBF; width:100%;"></div>
					</div>

					<div id="page_'.$pageId.'_edit_information" class="page-edit-information hidden">
						<div class="col-md-8" style="padding-left:0px; padding-right:0px;">
							<input id="page_'.$pageId.'_name" type="text" placeholder="Page Name" name="name" class="form-control" />
						</div>
						<div class="col-md-4" style="margin-top:5px;">
							<a id="page_'.$pageId.'_save_edit" class="confirm-button" data-placement="top" data-original-title="Save" data-toggle="tooltip"><i class="fa fa-check"></i></a>
							<a id="page_'.$pageId.'_cancel_edit" data-placement="top" class="delete-button" data-original-title="Cancel" data-toggle="tooltip"><i class="fa fa-times"></i></a>
						</div>
						<div class="col-md-12" style="margin-top:10px; margin-bottom:10px; border-bottom:1px solid #BFBFBF; width:100%;"></div>
					</div>
				</div>
			';

			$js = '
				$("#page_'.$pageId.'_name").on("click", function() {
					console.log("load page form");
					var post = {
						"action": "load_page_form",
						"id"    : '.$pageId.',
						"pid"	: '.$formId.'
		            };
		            trackplus.request.ajax_request("page", post);
				});

				$("#page_'.$pageId.'_edit").on("click", function() {
					$("#page_'.$pageId.'_information").addClass("hidden");
					$("#page_'.$pageId.'_edit_information").removeClass("hidden");
					$("#page_'.$pageId.'_edit_information input").val("'.$page->name.'");
				});

				$("#page_'.$pageId.'_cancel_edit").on("click", function() {
					$("#page_'.$pageId.'_edit_information").addClass("hidden");
					$("#page_'.$pageId.'_information").removeClass("hidden");
				});

				$("#page_'.$pageId.'_save_edit").on("click", function() {
					var post = {
						"pid"	: '.$formId.',
		                "id"    : '.$pageId.',
		                "action": "edit",
		                "name"	: $("#page_'.$pageId.'_edit_information input").val()
		            };
		            trackplus.request.ajax_request("page", post);
				});

				$("#page_'.$pageId.'_delete").on("click", function() {
					var post = {
						"action": "delete",
						"id"	: '.$pageId.'
					};
					trackplus.request.ajax_request("page", post);
				});
			';
			addDomreadyJS(renderHTMLString($js));
		}

		return renderHTMLString($html);
	}

	public function renderFormOptionButton($formId, $pageId = 0)
	{
		$options = array(
				'tpText'		=> 'Text Input',
				'tpTextarea'	=> 'Textarea',
				'tpCheckbox'	=> 'Checkbox',
				'tpRadio'		=> 'Radio',
				'tpDropdown'	=> 'Dropdown',
				'tpImage'		=> 'Image'
			);

		$formOptHtml = '';

		foreach ($options as $key => $text) {
			$formOptHtml .= '
				<div class="form-btn col-md-2" style="padding:0 5px;">
					<div id="'.$key.'" class="grey-btn-square border  item-element">
						<strong>'.$text.'</strong>
					</div>
				</div>
			';

			$type = strtolower(substr($key, 2));
			$js = '
				$("#'.$key.'").on("click", function() {
					var post = {
			            "action": "show_blank_option",
			            "id"    : '.$formId.',
			            "pid"	: '.$pageId.',
			            "type"	: "'.$type.'",
			        };
			        trackplus.request.ajax_request("form_item", post);
				});
			';

			addDomreadyJS($js);
		}

		return renderHTMLString($formOptHtml);
	}

	public function renderOptionModal($formId, $pageId, $itemId, $type)
	{
		$functionName = "renderOption".propercase($type);
		if (method_exists($this, $functionName)) {
			$modalContent = $this->{$functionName}($formId, $pageId, $itemId);
			$data = array(
					'type'		=> propercase($type),
					'content' 	=> $modalContent,
					'showSave'	=> true
				);
		} else {
			$modalContent = propercase($type).' option is not ready.';
			$data = array(
					'type'		=> propercase($type),
					'content' 	=> $modalContent,
					'showSave'	=> false
				);
		}
		// return renderHTMLString($this->_ci->load->view('form/modal', $data, true));
		return $this->_ci->load->view('form/modal', $data, true);
	}

	public function renderOptModalJS($formId, $pageId, $type, $itemId = 0)
	{
		$modalHtml = $this->renderOptionModal($formId, $pageId, $itemId, $type);
		$js = '
			var fModal = $.parseHTML("'.addslashes($modalHtml).'");
			$(fModal).modal("show")
			.on("hidden.bs.modal", function() {
				console.log("remove modal");
				$(this).remove();
			})
			.on("shown.bs.modal", function() {
				option = $("#itemOption").val();
				if (option) {
					option = option.replace(/<tp_nl>/g, "\n");
					$("#itemOption").val(option);
				}

				$("#formItemSave").on("click", function() {
					var post = {
						"action"	: "save",
						"formId"	: '.$formId.',
						"pageId"	: '.$pageId.',
						"itemId"	: '.$itemId.',
						"type"		: "'.$type.'"
					};
					post = $.param(post) + "&" + $("#itemOptionForm").serialize();
					trackplus.request.ajax_request("form_item", post);
				});
			});
		';
		return renderHTMLString($js);
	}

	public function renderOptModalHtml($itemName, $itemCaption, $itemOrder, $itemCompulsory, $optionHtml = '')
	{
		$itemNameHtml = '
			<div class="form-group">
                <label for="itemName">Item Name</label>
                <input type="text" class="form-control" name="itemName" id="itemName" value="'.$itemName.'">
            </div>
		';

		$itemCaptionHtml = '
			<div class="form-group">
                <label for="itemCaption">Item Caption</label>
                <input type="text" class="form-control" name="itemCaption" id="itemCaption" value="'.$itemCaption.'">
            </div>
		';

		$itemOrderHtml = '';
		/*$itemOrderHtml = '
			<div class="form-group">
                <label for="itemOrder">Item Order</label>
                <input type="number" class="form-control" name="itemOrder" id="itemOrder" min="1" value="'.$itemOrder.'">
            </div>
		';*/

		$itemCompulsoryHtml = '
			<div class="form-group">
                <div><label for="isCompulsory">Is Compulsory?</label></div>
                <div class="btn-group" data-toggle="buttons" id="isCompulsory">
                    <label id="isCompulsoryYes" class="btn btn-option'.($itemCompulsory ? ' active' : '').'" autocomplete="off">
                    <input id="isCompulsoryYes" type="radio" name="isCompulsory" value="1"'.($itemCompulsory ? 'checked=""' : '').'> Yes</label>
                    <label id="isCompulsoryNo" class="btn btn-option'.(!$itemCompulsory ? ' active' : '').'" autocomplete="off">
                    <input id="isCompulsoryNo" type="radio" name="isCompulsory" value="0" '.(!$itemCompulsory ? 'checked=""' : '').'> No</label>
                </div>
            </div>
		';

		return renderHTMLString($itemNameHtml . $itemCaptionHtml . $itemOrderHtml . $optionHtml . $itemCompulsoryHtml);
	}

	public function renderOptionText($formId, $pageId, $itemId = 0)
	{
		$itemName = '';
		$itemCaption = '';
		$itemOrder = $this->_ci->form_item_factory->getMaxSequence($formId, $pageId);
		$itemCompulsory = 1;

		if ($itemId > 0) {
			$item = $this->_ci->form_item_factory->getOne($itemId);
			$itemName = $item->name;
			$itemCaption = $item->caption;
			$itemOrder = $item->sequence;
			$itemCompulsory = $item->isCompulsory;
		}

		return $this->renderOptModalHtml($itemName, $itemCaption, $itemOrder, $itemCompulsory);
	}

	public function renderOptionTextarea($formId, $pageId, $itemId = 0)
	{
		$itemName = '';
		$itemCaption = '';
		$itemOrder = $this->_ci->form_item_factory->getMaxSequence($formId, $pageId);
		$itemCompulsory = 1;

		if ($itemId > 0) {
			$item = $this->_ci->form_item_factory->getOne($itemId);
			$itemName = $item->name;
			$itemCaption = $item->caption;
			$itemOrder = $item->sequence;
			$itemCompulsory = $item->isCompulsory;
		}

		return $this->renderOptModalHtml($itemName, $itemCaption, $itemOrder, $itemCompulsory);
	}

	public function renderOptionCheckbox($formId, $pageId, $itemId = 0)
	{
		$itemName = '';
		$itemCaption = '';
		$itemOrder = $this->_ci->form_item_factory->getMaxSequence($formId, $pageId);
		$itemCompulsory = 1;
		$options = '';
		$row = 3;

		if ($itemId > 0) {
			$item = $this->_ci->form_item_factory->getOne($itemId);
			$itemName = $item->name;
			$itemCaption = $item->caption;
			$itemOrder = $item->sequence;
			$itemCompulsory = $item->isCompulsory;

			$dataStore = unserialize($item->dataStore);
			$values = $dataStore['values'];
			if ($row < sizeof($values)) {
				$row = sizeof($values);
			}
			foreach ($values as $key => $value) {
				$options .= $value;
				if ($key < sizeof($values) - 1) {
					$options .= '<tp_nl>';
				}
			}
		}

		// Build options html
		$optionHtml = '
			<div class="form-group">
				<label for="itemOption">Options (separate each option with new line)</label>
				<textarea name="options" class="form-control" id="itemOption" style="resize: vertical;" rows="'.$row.'">'.$options.'</textarea>
			</div>
		';

		return $this->renderOptModalHtml($itemName, $itemCaption, $itemOrder, $itemCompulsory, $optionHtml);
	}

	public function renderOptionRadio($formId, $pageId, $itemId = 0)
	{
		$itemName = '';
		$itemCaption = '';
		$itemOrder = $this->_ci->form_item_factory->getMaxSequence($formId, $pageId);
		$itemCompulsory = 1;
		$options = '';
		$row = 3;

		if ($itemId > 0) {
			$item = $this->_ci->form_item_factory->getOne($itemId);
			$itemName = $item->name;
			$itemCaption = $item->caption;
			$itemOrder = $item->sequence;
			$itemCompulsory = $item->isCompulsory;

			$dataStore = unserialize($item->dataStore);
			$values = $dataStore['values'];
			if ($row < sizeof($values)) {
				$row = sizeof($values);
			}
			foreach ($values as $key => $value) {
				$options .= $value;
				if ($key < sizeof($values) - 1) {
					$options .= '<tp_nl>';
				}
			}
		}

		// Build options html
		$optionHtml = '
			<div class="form-group">
				<label for="itemOption">Options (separate each option with new line)</label>
				<textarea id="itemOption" name="options" class="form-control" style="resize: vertical;" rows="'.$row.'">'.$options.'</textarea>
			</div>
		';

		return $this->renderOptModalHtml($itemName, $itemCaption, $itemOrder, $itemCompulsory, $optionHtml);
	}

	public function renderOptionDropdown($formId, $pageId, $itemId = 0)
	{
		$itemName = '';
		$itemCaption = '';
		$itemOrder = $this->_ci->form_item_factory->getMaxSequence($formId, $pageId);
		$itemCompulsory = 1;
		$options = '';
		$row = 3;

		if ($itemId > 0) {
			$item = $this->_ci->form_item_factory->getOne($itemId);
			$itemName = $item->name;
			$itemCaption = $item->caption;
			$itemOrder = $item->sequence;
			$itemCompulsory = $item->isCompulsory;

			$dataStore = unserialize($item->dataStore);
			$values = $dataStore['values'];
			if ($row < sizeof($values)) {
				$row = sizeof($values);
			}
			foreach ($values as $key => $value) {
				$options .= $value;
				if ($key < sizeof($values) - 1) {
					$options .= '<tp_nl>';
				}
			}
		}

		// Build options html
		$optionHtml = '
			<div class="form-group">
				<label for="itemOption">Options (separate each option with new line)</label>
				<textarea id="itemOption" name="options" class="form-control" style="resize: vertical;" rows="'.$row.'">'.$options.'</textarea>
			</div>
		';

		return $this->renderOptModalHtml($itemName, $itemCaption, $itemOrder, $itemCompulsory, $optionHtml);
	}

	public function renderReorderItemsHtml($formId, $pageId) {
		$result = '<div id="reorderItemWrapper" class="pull-right">';
		$result .= '<a id="reorderItem" class="btn-text"><i class="fa fa-sort"></i>Reorder Item</a>';
		$result .= '<a id="saveReorderItem" class="btn-text hidden"><i class="fa fa-save"></i>Save Item Order</a>';
		$result .= '</div>';

		$js = '
			$("#reorderItem").on("click", function() {
				$("#reorderItem").addClass("hidden");
		        $("#saveReorderItem").removeClass("hidden");

		        $("#formDisplay").sortable({
		            cursor: "move",
		            items: "div.row"
		        });
			});
			$("#saveReorderItem").on("click", function() {
				console.log("Save reorder");
				var post = {
				    "action": "reorder",
				    "id"    : '.$formId.',
				    "pageId": '.$pageId.'
				};
				post = $.param(post) + "&" + $.param($("#formDisplay input[name=\"sequence[]\"]"));
				trackplus.request.ajax_request("form_item", post);
			});
		';

		addDomreadyJS($js);
		return $result;
	}

	public function renderItemsHtml($formId, $pageId, $builder = true, $preview = true)
	{
		$items = $this->_ci->form_item_factory->get($formId, $pageId);
		$result = '';
		if ($builder) {
		}
		if ($items) {
			foreach ($items as $item) {
				$result .= $this->renderItemRow($item->id, $preview);
			}
		}
		return $result;
	}

	public function renderItemRow($itemId, $preview = true)
	{
		$item = $this->_ci->form_item_factory->getOne($itemId);

		$functionName = "renderItem".propercase($item->type);
		$itemRow = '<div id="item_'.$item->id.'_row" class="row">';
		if ($preview) {
			$itemRow .= '<input type="hidden" name="sequence[]" value="'.$item->id.'"/>';
			$itemRow .= '<div class="col-md-12 valign item-row">';
			$itemRow .= '<div class="col-md-10">';
			$itemRow .= $this->renderFormGroup($item);
			$itemRow .= '</div>';
			$itemRow .= '<div class="col-md-2 text-right action-btn">';
			$itemRow .= '<a id="item_'.$itemId.'_delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete" class="pull-right"><i class="fa fa-trash"></i></a>';
			$itemRow .= '<a id="item_'.$itemId.'_edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit" class="pull-right"><i class="fa fa-pencil"></i></a>';
			$itemRow .= '</div>';
			$itemRow .= '</div>';

			$js = '
				$("#item_'.$itemId.'_edit").on("click", function() {
					var post = {
						"action"	: "show_option",
						"type"		: "'.$item->type.'",
						"formId"	: '.$item->formId.',
						"pageId"	: '.$item->pageId.',
						"itemId"	: '.$itemId.',
					};
					trackplus.request.ajax_request("form_item", post);
				});
				$("#item_'.$itemId.'_delete").on("click", function() {
					var post = {
						"action"	: "delete",
						"formId"	: '.$item->formId.',
						"pageId"	: '.$item->pageId.',
						"itemId"	: '.$itemId.',
					};
					trackplus.request.ajax_request("form_item", post);
				});
			';
			addDomreadyJS($js);
		} else {
			$itemRow .= $this->renderFormGroup($item);
		}
		$itemRow .= '</div>';
		return renderHTMLString($itemRow);
	}

	private function renderFormGroup($item) {
		$html = '';
		$functionName = "renderItem".propercase($item->type);
		if (method_exists($this, $functionName)) {
			$compulsory = '';
			$compulsoryHtml = '';
			if ($item->isCompulsory) {
				$compulsory = ' is-compulsory';
				$compulsoryHtml = ' <span style="color:red;">*</span>';
			}
			$html = '<div class="form-group'.$compulsory.'">';
			$itemCaption = '';
			if ($item->caption != '') {
				$itemCaption = ' <span class="item-caption">('.$item->caption.')</span>';
			}
			if ($item->name != '') {
				$html .= '<label for="item_'.$item->id.'_text">'.$item->name.$itemCaption.$compulsoryHtml.'</label>';
			}
			$html .= $this->{$functionName}($item);
			$html .= '</div>';
		} else {
			$html = propercase($item->type).' is not ready.';
		}
		return renderHTMLString($html);
	}

	public function renderItemText($item)
	{
		$html = '<input type="text" id="item_'.$item->id.'_text" name="item['.$item->pageId.']['.$item->id.']" class="form-control"/>';
		return renderHTMLString($html);
	}

	public function renderItemTextarea($item)
	{
		$html = '<textarea id="item_'.$item->id.'_input" name="item['.$item->pageId.']['.$item->id.']" class="form-control" style="resize: vertical;" rows="5"></textarea>';
		return renderHTMLString($html);
	}

	public function renderItemCheckbox($item)
	{
		$html = '';
		$dataStore = unserialize($item->dataStore);
		foreach ($dataStore['values'] as $value) {
			$html .= '<div class="checkbox">';
			$html .= '<label><input name="item['.$item->pageId.']['.$item->id.'][]" type="checkbox" value="'.$value.'">'.$value.'</label>';
			$html .= '</div>';
		}

		return renderHTMLString($html);
	}

	public function renderItemRadio($item)
	{
		$html = '';
		$dataStore = unserialize($item->dataStore);
		foreach ($dataStore['values'] as $key => $value) {
			$checked = ($key<=0)?' checked' : '';
			$html .= '<div class="radio">';
			$html .= '<label><input name="item['.$item->pageId.']['.$item->id.']" type="radio" value="'.$value.'"'.$checked.'>'.$value.'</label>';
			$html .= '</div>';
		}

		return renderHTMLString($html);
	}

	public function renderItemDropdown($item)
	{
		$dataStore = unserialize($item->dataStore);
		$html = '<select id="item_'.$item->id.'_dd" name="item['.$item->pageId.']['.$item->id.']" class="form-control">';
		foreach ($dataStore['values'] as $value) {
			$html .= '<option value="'.$value.'">'.$value.'</option>';
		}
		$html .= '</select>';
		return renderHTMLString($html);
	}

	public function renderSubmissionForm($marketingId, $formId) {
		// Build Form
		$form = $this->_ci->form_factory->getOne($formId);
		if ($form) {
			$fieldsetHtml = '';
			if ($form->havePage) {
				$pages = $this->_ci->page_factory->get($formId);
				$lastPageIndex = sizeof($pages);
				foreach ($pages as $idx => $page) {
					$fieldsetHtml .= $this->renderFieldset($formId, $page->id, $page->name, $page->sequence);
				}
			} else {
				$fieldsetHtml .= $this->renderFieldset($formId, 0);
			}

			$html = '<form id="form_'.$formId.'_submission" method="POST" action="'.site_url('api/form/submit').'">';
			$html .= '<input type="hidden" name="marketing" value="'.$marketingId.'" />';
			$html .= '<input type="hidden" name="form" value="'.$formId.'" />';
			$html .= $fieldsetHtml;
			$html .= $this->renderSummary($formId, $form->havePage);
			$html .= '</form>';

			$js = '
				$("html").bind("keypress", function(e) {
					if(e.keyCode == 13) {
						return false;
					}
				});
			';
			addDomreadyJS($js);
			return $html;
		} else {
			return "Page Not Found!";
		}
	}

	public function renderFieldset($formId, $pageId, $pageName = '', $sequence = 0) {
		$html = '<fieldset>';
		if ($pageName != '') {
			$html .= '<h3 class="page-title">'.$pageName.'</h3>';
		}
		$html .= $this->renderItemsHtml($formId, $pageId, false, false);

		$js = '';
		if ($this->_ci->page_factory->havePrev($formId, $sequence)) {
			$html .= '<button id="btn_'.$formId.$pageId.'_prev" type="button" class="btn btn-prev">Prev</button>';
			$js .= '
				$("#btn_'.$formId.$pageId.'_prev").on("click", function() {
					console.log("prev");
					currentField = $(this).parents("fieldset");
					tpFe.show(currentField, "prev");
				});
			';
		}
		if ($this->_ci->page_factory->haveNext($formId, $sequence)) {
			$html .= '<button id="btn_'.$formId.$pageId.'_next" type="button" class="btn btn-next pull-right">Next</button>';
			$js .= '
				$("#btn_'.$formId.$pageId.'_next").on("click", function() {
					console.log("next");
					currentField = $(this).parents("fieldset");
					if (tpFe.validateSubmission(currentField)) {
						tpFe.show(currentField, "next");
					}
				});
			';
		} else {
			$html .= '<button id="btn_'.$formId.$pageId.'_finish" type="button" class="btn btn-finish pull-right">Finish</button>';
			$js .= '
				$("#btn_'.$formId.$pageId.'_finish").on("click", function() {
					console.log("finish");
					currentField = $(this).parents("fieldset");
					if (tpFe.validateSubmission(currentField)) {
						$("#summary .summary-value").each(function(i, field) {
							$("#form_'.$formId.'_submission").find("[name*=\""+$(field).attr("data-id")+"\"]").each(function(j, input) {
								var value = "";
								switch ($(input).attr("type")) {
									case "radio":
									case "checkbox":
										if ($(input).is(":checked")) {
											value = $(input).val().trim();
										}
										break;
									default:
										value = $(input).val().trim();
										break;
								}
								value = value.replace(/(\r?\n|\r)/g, "<br/>");
								if (value != "" && $(field).text() != "") {
									$(field).append("<br/>");
								}
								$(field).append(value);
							});
						});

						$("#summary .summary-value").each(function(i, field) {
							$("#form_'.$formId.'_submission").find("[name*=\""+$(field).attr("data-id")+"\"]").each(function(j, input) {
								if ($(field).text() == "") {
									$(field).text("-");
								}
							});
						});
						tpFe.show(currentField, "next");
					}
				});
			';
		}

		// Validation JS
		$js .= '
			tpFe = {};
			tpFe.validateSubmission = function(currentField) {
				var isValid = true;
				currentField.find("span.error").remove();
				currentField.find("div.row").removeClass("error");
				$(currentField).find("div.form-group.is-compulsory").each( function() {
					if ($(this).find("input[type=text]").length > 0) {
						/* Text Input */
						if ($(this).find("input[type=text]").val().trim() == "") {
							$(this).parents("div.row").addClass("error");
							tpFe.showError($(this).parents("div.row"));
							isValid = false;
						}
					} else if ($(this).find("textarea").length > 0) {
						/* Textarea */
						if ($(this).find("textarea").val().trim() == "") {
							$(this).parents("div.row").addClass("error");
							tpFe.showError($(this).parents("div.row"));
							isValid = false;
						}
					} else if ($(this).find("input[type=checkbox]").length > 0) {
						/* Checkbox */
						if ($(this).find("input[type=checkbox]:checked").length <= 0) {
							$(this).parents("div.row").addClass("error");
							tpFe.showError($(this).parents("div.row"));
							isValid = false;
						}
					} else if ($(this).find("input[type=radio]").length > 0) {
						/* Radio */
						if ($(this).find("input[type=radio]:checked").length <= 0) {
							$(this).parents("div.row").addClass("error");
							tpFe.showError($(this).parents("div.row"));
							isValid = false;
						}
					} else if ($(this).find("select").length > 0) {
						/* Dropdown */
						if ($(this).find("select").val().trim() == "") {
							$(this).parents("div.row").addClass("error");
							tpFe.showError($(this).parents("div.row"));
							isValid = false;
						}
					}
				});
				return isValid;
			};

			tpFe.showError = function(currentRow) {
				currentRow.find(".form-group").append("<span class=\"error\">This field is required</span>");
			};

			tpFe.show = function(field, type) {
				field.fadeOut(400, function() {
					if (type == "next" || type == "finish") {
						$(this).next().fadeIn();
					} else {
						$(this).prev().fadeIn();
					}
		    	});
			};

			$("fieldset:nth-child(3)").fadeIn();
		';

		$html .= '</fieldset>';
		addDomreadyJS($js);
		return $html;
	}

	public function renderSummary($formId, $havePage) {
		$items = $this->_ci->form_item_factory->getAll($formId, $havePage);
		$html = '<fieldset>';
		$html .= '<h3 class="page-title">Submission Summary</h3>';
		$html .= '<table id="summary">';
		if ($items) {
			foreach ($items as $item) {
				$html .= '<tr>';
				$html .= '<td>'.$item->name.'</td>';
				$html .= '<td class="text-center"> : </td>';
				$html .= '<td width="100%" class="summary-value" data-id="item['.$item->pageId.']['.$item->id.']"></td>';
				$html .= '</tr>';
			}
		}
		$html .= '</table>';
		$html .= '<button id="btn_back" type="button" class="btn btn-prev">Back</button>';
		$html .= '<button id="btn_save" type="button" class="btn btn-save pull-right">Save</button>';
		$html .= '</fieldset>';

		$js = '
			$("#btn_back").on("click", function() {
				var currentField = $(this).parents("fieldset");
				tpFe.show(currentField, "prev");
				currentField.find("table td.summary-value").empty();
			});

			$("#btn_save").on("click", function() {
				if (!$(this).hasClass("disabled")) {
					console.log("Button save clicked");
					$(this).addClass("disabled");
					$("form").submit();
				}
			});
		';

		addDomreadyJS($js);
		return $html;
	}
}