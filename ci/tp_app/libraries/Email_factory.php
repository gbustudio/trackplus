<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();

		$config = array(
		    'protocol' 	=> 'smtp',
		    'smtp_host' => 'mail.trackplus.co.id',
		    'smtp_port' => 25,
		    'smtp_user' => 'no-reply@trackplus.co.id',
		    'smtp_pass' => 'BfTK5;P#d^[,[CsQuI',
		    'mailtype'  => 'html',
		    'charset'   => 'iso-8859-1',
		    'crlf'		=> "\r\n",
		    'newline'		=> "\r\n"
		);

		$this->_ci->load->library('email', $config);
	}

	public function sendNewRegistration($email, $password) {
		$this->_ci->email->from('no-reply@trackplus.co.id', 'Trackplus');
		$this->_ci->email->reply_to('no-reply@trackplus.co.id', 'Do Not Reply');
		$this->_ci->email->to($email);

		$this->_ci->email->subject('New Registration');

		$message = $this->_ci->load->view('email', array('password' => $password), true);
		$this->_ci->email->message($message);

		$this->_ci->email->send();

		return true;
	}

}