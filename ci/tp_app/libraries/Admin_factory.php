<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();
	}

	public function getScreenName($adminId) {
		$query = $this->_ci->db->query('
				SELECT A.email email, AP.name name
				FROM '.TBL_ADMINS.' A, '.TBL_ADMIN_PROFILES.' AP
				WHERE A.id = AP.adminId AND
					  A.id = '.$this->_ci->db->escape($adminId).'
			');

		if ($query->num_rows() > 0) {
			$row = $query->row();

			if (strlen($row->name) > 0) {
				return $row->name;
			} else {
				return $row->email;
			}
		}

		return false;
	}
}