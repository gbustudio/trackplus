<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();
	}

	public function get($companyId) {
		$query = $this->_ci->db->get_where(TBL_MARKETINGS, array('companyId' => $companyId));
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}

	public function getOne($marketingId) {
		$query = $this->_ci->db->get_where(TBL_MARKETINGS, array('id' => $marketingId));
		if ($query->num_rows() == 1) {
			return $query->row();
		}
		return false;
	}

	public function get4Mobile($email) {
		$result = $this->_ci->db->query('
				SELECT M.id, M.companyId, C.name companyName, M.email, MP.name, MP.address, MP.phone, M.deletedAt
				FROM '.TBL_COMPANIES.' C, '.TBL_MARKETINGS.' M, '.TBL_MARKETING_PROFILES.' MP
				WHERE lower(M.email) = '.$this->_ci->db->escape($email).' AND
					  M.id = MP.marketingId AND
					  C.id = M.companyId
			');
		if ($result->num_rows() > 0) {
			return $result->row();
		}
		return false;
	}

}