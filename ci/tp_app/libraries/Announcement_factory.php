<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement_factory {
	private $_ci;

	function __construct() {
		$this->_ci =& get_instance();
	}

	public function get($companyId) {
		$result = $this->_ci->db->get(TBL_ANNOUNCEMENTS, array('companyId' => $companyId));
		if ($result->num_rows() > 0) {
			return $result;
		}
		return false;
	}

	public function getOne($announcementId) {
		$result = $this->_ci->db->get(TBL_ANNOUNCEMENTS, array('id' => $announcementId));
		if ($result->num_rows() > 0) {
			return $result->row();
		}
		return false;
	}

	public function get4Mobile($companyId) {
		$result = $this->_ci->db->query('
				SELECT id, title, content
				FROM '.TBL_ANNOUNCEMENTS.' A
				WHERE companyId = '.$this->_ci->db->escape($companyId).' AND
					  published = 1 AND
					  ISNULL(deletedAt)
			');
		if ($result->num_rows() > 0) {
			return $result->result();
		}
		return false;
	}

}