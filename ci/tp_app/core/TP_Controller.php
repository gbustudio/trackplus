<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TP_Controller extends CI_Controller {

	public function __construct($ctrl=null) {
		// in php if a variable is null, it's considered not set. is_null for double fail safe in case of changes in further php version
		if(!isset($ctrl) || is_null($ctrl)){
			exit('Illegal Bootstrapping');
		}

		parent::__construct();

		switch ($ctrl) {
			case 'Developer':
			case 'Authentication':
			case 'Announcement-Api':
			case 'Form-Api':
				break;
			case 'Request':
				break;
				if (strtolower($this->uri->segment(2)) == 'user') {
					break;
				}
			default:
				if (!$this->psecurity->isUserLoggedIn()) {
					redirect(site_url('authentication/login'));
				}
				break;
		}
	}
}

/* End of file WF_Controller.php */
/* Location: ./wwf_app/core/WF_Controller.php */