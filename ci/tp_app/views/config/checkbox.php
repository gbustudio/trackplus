<div class="modal fade" id="checkboxConfig" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Checkbox</h4>
            </div>
            <div class="modal-body">
              <div class=".grey-input-form-modal">
                  <div id="response_container"></div>
                  <form id="checkboxConfigForm" class="input-form">
                      <div class="form-group">
                          <label for="checkboxName">Item Name</label>
                          <input type="email" class="form-control" name="itemName" id="checkboxName">
                      </div>
                      <div class="form-group">
                          <label for="checkboxOrder">Order</label>
                          <input type="text" class="form-control" name="itemOrder" id="checkboxOrder">
                      </div>
                      <div class="form-group">
                          <label for="checkboxCaption">Caption</label>
                          <input type="text" class="form-control" name="itemCaption" id="checkboxCaption">
                      </div>
                      <div class="form-group">
                          <label for="checkboxOption">Options(separate each option by comma)</label>
                          <textarea class="form-control" id="checkboxOption"></textarea>
                      </div>
                      <div class="form-group">
                          <!-- <div class="compulsory-caption">is Compulsory?</div> -->
                          <input type="hidden" class="form-control" name="type">
                          <div><label for="textCompulsory">Is Compulsory?</label></div>
                          <div class="btn-group" data-toggle="buttons" id="textCompulsory">
                              <label id="textCompulsoryYes" class="btn btn-option active" autocomplete="off">
                              <input id="textCompulsoryYes" type="radio" name="isCompulsory" value="1" checked=""> Yes</label>
                              <label id="textCompulsoryNo" class="btn btn-option " autocomplete="off">
                              <input id="textCompulsoryNo" type="radio" name="isCompulsory" value="0"> No</label>
                          </div>
                      </div>
                  </form>
              </div>
            </div>
            <div class="modal-footer" style="background-color: #f5f5f5;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onclick="trackplus.project.add_mock_option('checkbox')">Save changes</button>
            </div>
        </div>
    </div>
</div>