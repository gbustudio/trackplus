<div class="modal fade" id="optionConfig" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Option</h4>
            </div>
            <div class="modal-body">
              <div class="grey-input-form">
                  <div id="response_container"></div>
                  <form id="ss" class="input-form">
                      <div class="form-group">
                          <label for="textName">Item Name</label>
                          <input type="email" class="form-control" name="itemName" id="textName">
                      </div>
                      <div class="form-group">
                          <label for="textOrder">Order</label>
                          <input type="text" class="form-control" name="itemOrder" id="textOrder">
                      </div>
                      <div class="form-group">
                          <label for="itemCaption">Caption</label>
                          <input type="text" class="form-control" name="itemCaption" id="itemCaption">
                      </div>
                      <div class="form-group">
                          <!-- <div class="compulsory-caption">is Compulsory?</div> -->
                          <input type="hidden" class="form-control" name="type">
                          <div><label for="textCompulsory">Is Compulsory?</label></div>
                          <div class="btn-group" data-toggle="buttons" id="textCompulsory">
                              <label id="textCompulsoryYes" class="btn btn-option active" autocomplete="off">
                              <input id="textCompulsoryYes" type="radio" name="isCompulsory" value="1" checked=""> Yes</label>
                              <label id="textCompulsoryNo" class="btn btn-option " autocomplete="off">
                              <input id="textCompulsoryNo" type="radio" name="isCompulsory" value="0"> No</label>
                          </div>
                      </div>
                  </form>
              </div>
            </div>
            <div class="modal-footer" style="background-color: #f5f5f5;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" >Save changes</button>
            </div>
        </div>
    </div>
</div>