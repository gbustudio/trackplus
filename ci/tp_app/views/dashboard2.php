<!-- <div>
	<h1>TRACKPLUS DASHBOARD</h1>
</div> -->
<div class="content-title col-md-12">
	<span class="main-title">Dashboard</span>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div class="col-md-4 col-xs-12">
		<div class="col-md-12 card">
			<div class="col-md-12 text-center card-header">Marketings</div>
			<div class="col-md-12 text-center card-main-info">10/10</div>
			<div class="col-md-12 text-center card-sub-info">10 Actives Marketings From 10 Marketings</div>
		</div>
	</div>
	<div class="col-md-4 col-xs-12">
		<div class="col-md-12 card">
			<div class="col-md-12 text-center card-header">Announcements</div>
			<div class="col-md-12 text-center card-main-info">2/5</div>
			<div class="col-md-12 text-center card-sub-info">2 Actives Announcements From 5 Announcements</div>
		</div>
	</div>
	<div class="col-md-4 col-xs-12">
		<div class="col-md-12 card">
			<div class="col-md-12 text-center card-header">Customers</div>
			<div class="col-md-12 text-center card-main-info">30</div>
			<div class="col-md-12 text-center card-sub-info">30 Customers Visited Today</div>
		</div>
	</div>
	<div class="col-md-12 no-padding" style="margin-top: 15px;">
		<div class="col-md-12">
			<div class="col-md-12 bottom-border no-padding">
				<span class="dashboard-sub-title">Activities</span>
			</div>
		</div>
	</div>
</div>

