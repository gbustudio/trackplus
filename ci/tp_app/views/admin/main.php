	<div class="content-title col-md-12">
		<span class="main-title">Admin List</span>
	</div>
	<div class="col-md-12 button-add">
		<a href="<?php echo site_url('admin/manage/add'); ?>" class="grey-btn-square border"><i class="fa fa-plus"></i><strong>Add New Admin</strong></a>
	</div>
	<div class="col-md-12">
		<div class="data-list-box border">
			<div class="admin-table white-table">
				<table id="admin_table" class="display" cellspacing="0" width="100%"></table>
			</div>
		</div>
	</div>