<div class="content-title col-md-12">
	<div class="col-md-6"><h3>CUSTOMER / <?php echo strtoupper($action) ?></h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction; ?>"> Save</a>
	</div>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div id="response_container"></div>
	<form id="customer" class="input-form">
		<div class="form-group">
			<label for="name">Name<span class="required">*</span></label>
			<input type="text" class="form-control" name="name" id="name" value="<?php echo $name; ?>" >
		</div>
		<div class="form-group">
			<label for="phone">Phone No.<span class="required">*</span></label>
			<input type="tel" class="form-control" name="phone" id="phone" onkeydown="" value="<?php echo $phone; ?>">
		</div>
		<div class="form-group">
			<label for="fax">Fax No.</label>
			<input type="tel" class="form-control" name="fax" id="fax" onkeydown="" value="<?php echo $fax; ?>">
		</div>
		<div class="form-group">
			<label for="address">Address</label>
			<textarea type="text" class="form-control" name="address" id="address"><?php echo $address;?></textarea>
		</div>
		<div class="form-group">
			<label for="cpName">Contact Person Name</label>
			<input type="text" class="form-control" name="cpName" id="cpName" onkeydown="" value="<?php echo $cpName; ?>">
		</div>
		<div class="form-group">
			<label for="cpEmail">Contact Person Email</label>
			<input type="email" class="form-control" name="cpEmail" id="cpEmail" onkeydown="" value="<?php echo $cpEmail; ?>">
		</div>
		<div class="form-group">
			<label for="cpPhone">Contact Person Phone No.</label>
			<input type="tel" class="form-control" name="cpPhone" id="cpPhone" onkeydown="" value="<?php echo $cpPhone; ?>">
		</div>
	</form>
</div>


