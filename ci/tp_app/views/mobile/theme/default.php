<style type="text/css">
	html, body {
		height: 100%;
		width: 100%;
	}
	span.error {
		color: red;
	}
	.form-group {
		margin-bottom: 5px;
	}
	fieldset {
		display: none;
	}
	table {
	    border-collapse: collapse;
	}
	td {
		vertical-align: top;
		padding-top: .5em;
		padding-bottom: .5em;
		padding-right: .5em;
	}

	.wrapper {
		position: relative;
		height: 100%;
	}
	.row {
		padding-left: 10px;
		padding-right: 10px;
		margin-bottom: 5px;
	}
	.page-title {
		text-align: center;
	}
	.row.error {
		background: rgba(255,0,0,0.1);
	}
	.btn-next,
	.btn-next:hover,
	.btn-next:focus,
	.btn-next:active {
		color: white;
		background: green;
	}
	.btn-prev,
	.btn-prev:hover,
	.btn-prev:focus,
	.btn-prev:active {
		color: white;
		background: green;
	}
	.btn-finish,
	.btn-finish:hover,
	.btn-finish:focus,
	.btn-finish:active {
		color: white;
		background: green;
	}

	#announcement_wrapper {
		width: 100%;
		height: 100%;
		overflow: hidden;
	}
	#inner_wrapper {
		position: relative;
		padding: 0;
		height: 100%;
	}
	#inner_wrapper div.left, #inner_wrapper div.right {
		height: 100%;
		margin: 0;
		padding: 0 5px;
		text-align: center;
	}
	#inner_wrapper .announcement-content {
		height: 80%;
		display: none;
	}
	#content_wrapper {
		height: 100%;
		overflow: hidden;
	}
	div.left a, div.right a {
		position: relative;
		top: 50%;
	}
	div.content-wrapper {
		position: relative;
		height: 100%;
	}
	div.title {
		border-bottom: 1px solid #000000;
	}
	div.body {
		overflow-y: auto;
		height: 100%;
	}
	.btn.btn-nav {
		margin-top: 20px;
		margin-bottom: 10px;
		padding-top: 0px;
		padding-bottom: 0px;
	}
	.unselectable {
	    -webkit-touch-callout: none;
	    -webkit-user-select: none;
	    -khtml-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    -o-user-select: none;
	    user-select: none;
	}
</style>