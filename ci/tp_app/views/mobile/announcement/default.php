<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="author" content="Antonius Henry Setiawan" />
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Announcement</title>

		<link rel="stylesheet" href="<?php echo lib_url('ext/bootstrap/css/bootstrap.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/font-awesome/css/font-awesome.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/jquery-ui/jquery-ui.min.css');?>" type="text/css" charset="utf-8"/>
		<!-- Custom Style -->
		<?php echo $style; ?>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

	</head>

	<body>
		<div id="announcement_wrapper" class="container-fluid">
			<?php if($announcements): ?>
			<div id="inner_wrapper" class="row">
				<div class="col-xs-1 left">
					<a href="#" onclick="trackplus.announcement.left()"><i class="fa fa-arrow-left fa-2x"></i></a>
				</div>
				<div id="content_wrapper" class="col-xs-10 unselectable">
					<?php foreach($announcements as $key => $announcement): ?>
						<div id="announcement_<?php echo $key+1; ?>" class="announcement-content">
							<div class="title text-center">
								<h3><?php echo $announcement->title; ?></h3>
							</div>
							<div class="body">
								<?php echo $announcement->content; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="col-xs-1 right">
					<a href="#" onclick="trackplus.announcement.right()"><i class="fa fa-arrow-right fa-2x"></i></a>
				</div>
			</div>
			<?php else: ?>
				<div>No Announcement</div>
			<?php endif; ?>
		</div>

		<script type="text/javascript" src="<?php echo lib_url('ext/jquery/jquery-1.11.3.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo lib_url('ext/jquery-ui/jquery-ui.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo lib_url('ext/jquery.touchSwipe.min.js'); ?>"></script>
		<script>
			var baseURL = "<?php echo base_url(); ?>";
			$(function(){
				try{
					trackplus = {};

					$("#content_wrapper").swipe({
						swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
							switch (direction) {
								case "left":
									trackplus.announcement.right();
									break;
								case "right":
									trackplus.announcement.left();
									break;
								default:
									break;
							}
						},
						threshold: 0,
						allowPageScroll: "auto"
					});

					trackplus.announcement = {
						left: function() {
							var idx = $('.announcement-content:visible').prev('.announcement-content').index() + 1;
							if (idx == 0) {
								idx = $('.announcement-content').length;
							}
							this.show(idx, 'left');
							// console.log($('.announcement-content:visible').prev().index());
						},
						right: function() {
							var idx = $('.announcement-content:visible').next().index() + 1;
							if (idx == 0) {
								idx = 1;
							}
							this.show(idx, 'right');
							// console.log($('.announcement-content:visible').next().index());
						},
						init: function() {
							$("div.announcement-content:nth-child(1)").show("slide", { direction: "right" }, 500);
						},
						show: function(idx, direction) {
							$('.announcement-content:visible').hide();
							$("div.announcement-content:nth-child("+idx+")").show("slide", { direction: direction }, 500);
						}
					};

					<?php printDomreadyJS(); ?>
					console.log('DOM Ready JS Execution Complete');
				} catch(err) {
					console.log(err);
				}
			});
		</script>
	</body>
</html>