<div class="content-title col-md-12">
	<div class="col-md-6"><h3>Change Password</h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction; ?>">Save</a>
	</div>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div id="response_container"></div>
	<form id="profile" class="input-form">
		<!-- <div class="form-group" title="Email"> -->
		<div class="form-group">
			<label for="current">Current <span style="color:red">*</span></label>
			<input type="password" class="form-control" name="current" id="current" value="">
		</div>
		<div class="form-group">
			<label for="new">New <span style="color:red">*</span></label>
			<input type="password" class="form-control" name="new" id="new" value="">
		</div>
		<div class="form-group">
			<label for="retypeNew">Retype New <span style="color:red">*</span></label>
			<input type="password" class="form-control" name="retypeNew" id="retypeNew" value="">
		</div>
	</form>
</div>


