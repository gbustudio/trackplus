<div class="content-title col-md-12">
	<div class="col-md-6"><h3>ANNOUNCEMENT / MANAGE</h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction; ?>">Save</a>
		<a id="btn_publish_announcement" class="btn btn-success btn-sm <?php echo ($action == "add") ? "disabled" : "" ?>"><?php echo ($published) ? "Unpublish" : "Publish" ?></a>
	</div>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div id="response_container"></div>
	<form id="add_announcement" class="input-form">
		<div class="form-group">
			<label for="announcementTitle">Title</label>
			<input type="text" class="form-control" name="title" id="announcementTitle" value="<?php echo $title; ?>" />
		</div>
		<div class="form-group">
			<label for="announcementEditor">Content</label>
			<textarea name="announcementEditor" id="announcementEditor" rows="10" cols="80">
	        	<?php echo $content; ?>
	    	</textarea>
		</div>
		<!--
    	<div class="form-group">
			<label>Projects</label>
			<div id="projectContainer" class="project-container">
				<div class="col-md-12 no-padding">
					<input id="selectAll" type="checkbox" value=""/>
					<label for="selectAll" class="normal">
						<span>Select All</span>
					</label>
				</div>
				<?php foreach($projects as $project): ?>
					<div class="col-md-3 no-padding">
						<input type="checkbox" id="project_<?php echo $project->id; ?>_wrapper" name="projects[]" value="<?php echo $project->id; ?>" <?php if (in_array($project->id, $selectedProjects)) echo "checked"; ?>/>
						<label for="project_<?php echo $project->id; ?>_wrapper" class="normal">
							<span class="project-name"><?php echo $project->name; ?></span>
						</label>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		-->
	</form>
</div>