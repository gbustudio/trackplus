		<div class="container">
			<div class="logo col-md-12" style="position:relative">
				<div class="ajax-call-container hidden">
	                <img src="/assets/loading_spinner.gif"/>
	            </div>
			</div>

			<div class="logo col-md-12">
			  	<div class="center-block logo-container">
			  		<img src="<?php echo assets_url('trackplus.png') ?>" width="100px;">
			  	</div>
			</div>

			<div class="col-md-12">
				<div class="login-form col-md-4 col-md-offset-4">
					<div id="response_container"></div>
					<form>
						<div class="form-group">
							<input type="email" class="form-control" name="userEmail" placeholder="Email">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="userPassword" placeholder="Password">
						</div>

						<a class="btn btn-danger btn-block" onclick="trackplus.authentication.login($(this))">Sign in</a>
					</form>
					<!-- <div class="separator">
						<span class="separator-text">OR SIGN IN WITH</span>
					</div> -->
				</div>
			</div>
		</div>