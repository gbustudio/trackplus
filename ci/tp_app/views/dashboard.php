<!-- <div>
	<h1>TRACKPLUS DASHBOARD</h1>
</div> -->
<div class="content-title col-md-12">
	<span class="main-title">Dashboard</span>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12" style="margin-top:15px;">
	<div class="col-md-4"  style="text-align:center">
		<div class="col-md-12"><strong>Project</strong></div>
		<div class="col-md-12 border" style="background:#f5f5f5; border-color:#888888; padding-bottom:12px;">
			<!-- <span class="col-md-12" style="font-size:72px;">4/12</span> -->
			<span class="col-md-12" style="font-size:72px;"><?php echo $activeProject ?>/<?php echo $projectCount ?></span>
			<div class="double-separator col-md-12 no-margin"></div>
			<span style="color:green"><?php echo $activeProject ?> active projects from <?php echo $projectCount ?> projects</span>
		</div>
	</div>
	<div class="col-md-4" style="text-align:center">
		<div class="col-md-12" ><strong>Marketing</strong></div>
		<div class="col-md-12 border" style="background:#f5f5f5; border-color:#888888; padding-bottom:12px;">
			<span class="col-md-12" style="font-size:72px;"><?php echo $activeMarketing ?>/<?php echo $marketingCount ?></span>
			<div class="double-separator col-md-12 no-margin"></div>
			<span style="color:green"><?php echo $activeMarketing ?> active marketings from <?php echo $marketingCount ?> marketings</span>
		</div>
	</div>
	<div class="col-md-4 hidden" style="text-align:center">
		<div class="col-md-12"><strong>Project</strong></div>
		<div class="col-md-12 border" style="background:#f5f5f5; border-color:#888888;">
		</div>
	</div>
</div>

