<div class="content-title col-md-12">
	<div class="col-md-6"><h3>MARKETING / MANAGE</h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction;?>">Save</a>
	</div>
</div>

<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div id="response_container"></div>
	<form id="add_marketing" class="input-form">
		<!-- <div class="form-group" title="Email"> -->
		<div class="form-group">
			<label for="addEmail">Email<span class="required">*</span></label>
			<input type="email" class="form-control" name="email" id="addEmail" value="<?php echo $email; ?>"<?php echo ($action == 'edit') ? " disabled" : "" ?>>
		</div>
		<div class="form-group">
			<label for="addMarketingName">Name<span class="required">*</span></label>
			<input type="text" class="form-control" name="name" id="addMarketingName" value="<?php echo $name; ?>" >
		</div>
		<div class="form-group">
			<label for="addMarketingAddress">Address</label>
			<input type="text" class="form-control" name="address" id="addMarketingAddress" value="<?php echo $address; ?>">
		</div>
		<div class="form-group">
			<label for="addMarketingPhone">Mobile Phone</label>
			<input type="text" class="form-control" name="phone" id="addMarketingPhone" onkeydown="" value="<?php echo $phone; ?>">
		</div>

		<!-- <div class="form-group" style="text-align:right"></div> -->
	</form>
</div>


