<div class="content-title col-md-12">
	<div class="col-md-6"><h3>Form / <?php echo strtoupper($name) ?></h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction;?>">Publish</a>
	</div>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<!--
<div class="col-md-12">
	<div id="response_container"></div>
</div>
-->
<div id="projectContainer" class="col-md-12">
	<?php if ($havePage): ?>
	<div id="pageWrapper" class="col-md-4">
		<div id="form_page_header">
			<span class="">Page List</span>
			<a data-toggle="tooltip" data-placement="top" class="green-btn pull-right" data-original-title="Edit" onclick="trackplus.form.builder.addPage(<?php echo $formId; ?>)">
				<i class="fa fa-plus"></i>Add Page
			</a>
			<div class="double-separator col-md-12"></div>
		</div>

		<div id="reorderPageWrapper" class="wrapper-10 pull-right">
			<a id="reorderPage" class="btn-text" onclick="trackplus.form.builder.reorderPage();">
				<i class="fa fa-sort"></i>Reorder Page
			</a>

			<a id="saveReorderPage" class="btn-text hidden" onclick="trackplus.form.builder.saveReorderPage(<?php echo $formId; ?>);">
				<i class="fa fa-save"></i>Save Order
			</a>
		</div>

		<div id="form_page_input"></div>

		<div class="col-md-12" id="pageContainer">
			<?php echo $pageHtml; ?>
		</div>
	</div>
	<?php endif; ?>

	<!-- Form Builder Container -->
	<div class="col-md-<?php echo ($havePage ? "8" : "12"); ?>" id="buildFormContainer">
		<span>Form</span>
		<div class="double-separator col-md-12" style="margin: 0px 0px 15px 0px;"></div>

		<div id="formItemBtnContainer" class="row form-container">
			<?php if (!$havePage) echo $optsHtml; ?>
		</div>

		<div class="col-md-12 border" style="background:white; margin-top:15px; border-color: #888888;">
			<?php if ($havePage): ?>
			<div id="pageName" class="center text-center">
				<h4>Please Select a Page</h4>
			</div>
			<?php endif; ?>
			<?php if (!$havePage) echo $reorderItemsHtml; ?>
			<div class="col-md-12" style="background:white; padding:15px; text-align:left" id="formDisplay">
				<?php if (!$havePage) echo $itemsHtml; ?>
			</div>
		</div>
	</div>
</div>