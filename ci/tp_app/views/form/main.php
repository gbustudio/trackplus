	<div class="content-title col-md-12">
		<span class="main-title">Project List</span>
	</div>
	<div class="logo col-md-12" style="position:relative">
		<div class="ajax-call-container hidden">
            <img src="/assets/loading_spinner.gif"/>
        </div>
	</div>

	<div class="col-md-12 button-add">
		<a href="<?php echo site_url('form/manage/add'); ?>" class="grey-btn-square border"><i class="fa fa-plus"></i><strong style="font-family:'Ubuntu'">Add New Form</strong></a>
	</div>
	<div class="col-md-12">
		<div class="data-list-box border">
			<div class="project-table white-table">
				<table id="project_table" class="display" cellspacing="0" width="100%">
				</table>
			</div>
		</div>
	</div>


