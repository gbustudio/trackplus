<div class="modal fade" id="formItemOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f5f5f5;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $type; ?></h4>
            </div>
            <div class="modal-body">
                <div class=".grey-input-form-modal">
                    <div id="response_container"></div>
                    <form id="itemOptionForm" class="input-form">
                        <?php echo $content; ?>
                    </form>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #f5f5f5;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php if ($showSave): ?>
                <button id="formItemSave" type="button" class="btn btn-default" onclick="">Save changes</button>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>