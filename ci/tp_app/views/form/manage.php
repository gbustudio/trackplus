<div class="content-title col-md-12">
	<div class="col-md-6"><h3>Form / Manage</h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction;?>">Save</a>
	</div>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div id="response_container"></div>
	<form id="add_project" class="input-form">
		<div class="form-group">
			<label for="projectName">Name</label>
			<input type="text" class="form-control" name="name" id="projectName" value="<?php echo $name; ?>">
		</div>
		<div class="form-group">
			<label for="ProjectDescription">Description</label>
			<input type="text" class="form-control" name="description" id="ProjectDescription" value="<?php echo $desc; ?>">
		</div>
		<div class="form-group">
			<div><label>Have Page</label></div>
			<label class="radio-inline"><input type="radio" name="havePage" value="1"<?php if($havePage) echo " checked"; ?><?php echo ($action == "edit") ? " disabled" : "" ?>>Yes</label>
			<label class="radio-inline"><input type="radio" name="havePage" value="0"<?php if(!$havePage) echo " checked"; ?><?php echo ($action == "edit") ? " disabled" : "" ?>>No</label>
		</div>
		<div class="form-group">
			<!--
			<div><strong>Available Marketings</strong></div>
			<?php if ($marketings): ?>
			<?php foreach ($marketings as $marketing): ?>
				<div class="col-md-3 no-padding">
					<input type="checkbox" id="marketing_<?php echo $marketing->id; ?>_wrapper" name="marketings[]" value="<?php echo $marketing->id; ?>" <?php echo ($marketing->formId == $formId)?"checked":"" ?> />
					<label for="marketing_<?php echo $marketing->id; ?>_wrapper" class="normal">
						<span class="marketing-name"><?php echo $marketing->name; ?></span>
					</label>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				<div class="col-md-12 no-padding">
					No Marketings Available.
				</div>
			<?php endif; ?>
			-->
		</div>

		<!--
		<div class="form-group" style="text-align:right">
			<a class="btn-form-add no-underline " onclick="trackplus.project.<?php echo $btnAction; ?>"><?php echo ($action == 'add') ? "Simpan" : "Simpan Perubahan" ?></a>
		</div>
		-->
	</form>

</div>


