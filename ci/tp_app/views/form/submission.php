	<div class="content-title col-md-12">
		<span class="main-title">Submission / <?php echo $projectName ?></span>
	</div>
	<div class="logo col-md-12" style="position:relative">
		<div class="ajax-call-container hidden">
            <img src="/assets/loading_spinner.gif"/>
        </div>
	</div>
	<div id="buttonWrapper" class="col-md-12 button-add">
		<a href="<?php echo site_url('project/export_csv/'.$projectId); ?>" class="grey-btn-square border"><i class="fa fa-file-text-o"></i><strong style="font-family:'Ubuntu'">Export to CSV</strong></a>
	</div>
	<div class="col-md-12">
		<div class="data-list-box border">
			<div class="project-table white-table">
				<table id="submission_table" class="stripe hover nowrap" cellspacing="0" width="200%"></table>
			</div>
		</div>
	</div>


