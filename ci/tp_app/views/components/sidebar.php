
    <div class="row no-margin">
        <div id="trackplus-sidebar" class="col-sm-3 col-md-2 sidebar">
            <div class="sidebar-menu row" style="margin-left:1%;">
                <ul class="sidebar-list-menu">
                    <?php foreach($menus as $key => $val): ?>
                        <li>
                            <a href="<?php echo $val['url']; ?>">
                            <div class="side-row <?php echo (strtolower($key) == strtolower($title)) ? ' active ' : ''; ?>">
                                <?php if (isset($val['icon'])): ?>
                                <i class="<?php echo $val['icon']; ?> fa-fw" style="vertical-align: middle;"></i>
                                <?php endif;?>
                                <?php echo $val['label']; ?>
                            </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div id ="trackplus-content" class="col-md-10 col-md-offset-2 content-dashboard no-padding">
            <div class="row dashboard-content no-margin">
                <?php echo $html; ?>
            </div>
        </div>
    </div>



<!--
<ul class="sidebar-list-menu">
<li><a href="#"><div class="side-row active">item1</div></a></li>
<li><a href="#"><div class="side-row">item2</div></a></li>
<li><a href="#"><div class="side-row acc" data-toggle="collapse" data-target="#sidebar-acc">item3</div></a></li>
<div id="sidebar-acc" class="collapse out sidebar-acc-container">
<div class="sidebar-item-acc">item acc 1</div>
<div class="sidebar-item-acc">item acc 2</div>
<div class="sidebar-item-acc">item acc 3</div>
<div class="sidebar-item-acc">item acc 4</div>
</div>
<li><a href="#"><div class="side-row">item4</div></a></li>
<li><a href="#"><div class="side-row">item5</div></a></li>
</ul>
-->

<!-- <a href="#"><div class="side-row">item1</div></a>
<a href="#"><div class="side-row">item2</div></a>
<a href="#"><div class="side-row acc" data-toggle="collapse" data-target="#sidebar-acc">item3</div></a>
<div id="sidebar-acc" class="collapse out sidebar-acc-container">
<div class="sidebar-item-acc">item acc 1</div>
<div class="sidebar-item-acc">item acc 2</div>
<div class="sidebar-item-acc">item acc 3</div>
<div class="sidebar-item-acc">item acc 4</div>
<?php //echo $html; ?>
</div>
<a href="#"><div class="side-row">item4</div></a>
<a href="#"><div class="side-row">item5</div></a> -->
