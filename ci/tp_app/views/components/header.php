<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title; ?></title>

		<link rel="stylesheet" href="<?php echo lib_url('ext/bootstrap/css/bootstrap.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/font-awesome/css/font-awesome.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/jquery-ui/jquery-ui.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/datatables/datatables.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('css/style.css');?>" type="text/css" charset="utf-8"/>

	</head>

	<body class="background no-margin">
		<div class="container-fluid">