    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-brand no-padding">
                    <ul class="nav navbar-nav burger-left-ul" onclick="trackplus.sidebar.toggleSidebar($(this))">
                        <li><a class="burger-left"><i class="fa fa-bars"></i></a></li>
                    </ul>
                </div>
            </div>

            <div id="navbar" >
                <a class="nav navbar-nav navbar-logo" href="<?php echo site_url(); ?>" style="width:50%;text-align:right">
                    <div  class="no-padding" style="display:inline"> <img src="<?php echo assets_url('logo-white.png') ?>"  class="navbar-logo-image"></div>
                    <div  class="no-padding" style="display:inline"> <img src="<?php echo assets_url('font-white.png') ?>"  class="navbar-logo-font"></div>
                </a>

                <ul class="nav navbar-nav navbar-right trackplus-right-nav-icon">
                    <li class="dropdown">
                        <a class="dropdown-toggle" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user"></i>
                            <span id="screenName"><?php echo $screenName; ?></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="userMenu">
                            <li><a href="<?php echo site_url('profile?section=edit'); ?>"><i class="fa fa-pencil">&nbsp; Edit Profiles</i></a></li>
                            <li><a href="<?php echo site_url('profile?section=change-password'); ?>"><i class="fa fa-key">&nbsp; Change Password</i></a></li>
                            <li><div class="separator no-margin"></div></li>
                            <li><a onclick="trackplus.authentication.logout();"><i class="fa fa-sign-out">&nbsp; Logout</i></a></li>
                        </ul>
                    </li>
                <!-- <li><a href="#"><i class="fa fa-bars"></i></a></li> -->
                </ul>
            </div>
        </div>
    </nav>