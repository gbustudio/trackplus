<div class="content-title col-md-12">
	<div class="col-md-6"><h3>Edit Profiles</h3></div>
	<div class="col-md-6 text-right">
		<a class="btn btn-success btn-sm" onclick="<?php echo $btnAction; ?>">Save</a>
	</div>
</div>
<div class="logo col-md-12" style="position:relative">
	<div class="ajax-call-container hidden">
        <img src="/assets/loading_spinner.gif"/>
    </div>
</div>
<div class="col-md-12">
	<div id="response_container"></div>
	<form id="profile" class="input-form">
		<!-- <div class="form-group" title="Email"> -->
		<div class="form-group">
			<label for="adminEmail">Email <span style="color:red">*</span></label>
			<input type="email" class="form-control" name="email" id="adminEmail" value="<?php echo $email; ?>" disabled>
		</div>
		<div class="form-group">
			<label for="adminName">Name</label>
			<input type="text" class="form-control" name="name" id="adminName" value="<?php echo $name; ?>" >
		</div>
		<div class="form-group">
			<label for="adminAddress">Address</label>
			<input type="text" class="form-control" name="address" id="adminAddress" value="<?php echo $address; ?>">
		</div>
		<div class="form-group">
			<label for="adminPhone">Mobile Phone</label>
			<input type="text" class="form-control" name="phone" id="adminPhone" onkeydown="" value="<?php echo $phone; ?>">
		</div>
	</form>
</div>


