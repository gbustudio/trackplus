<style type="text/css">
	.row {
		padding-left: 10px;
		padding-right: 10px;
		margin-bottom: 5px;
	}
	.page-title {
		text-align: center;
	}
	.row.error {
		background: rgba(255,0,0,0.1);
	}
	span.error {
		color: red;
	}
	.form-group {
		margin-bottom: 5px;
	}
	fieldset {
		display: none;
	}
	table {
	    border-collapse: collapse;
	}
	td {
		vertical-align: top;
		padding-top: .5em;
		padding-bottom: .5em;
		padding-right: .5em;
	}
	.btn-next,
	.btn-next:hover,
	.btn-next:focus,
	.btn-next:active {
		color: white;
		background: green;
	}
	.btn-prev,
	.btn-prev:hover,
	.btn-prev:focus,
	.btn-prev:active {
		color: white;
		background: green;
	}
	.btn-finish,
	.btn-finish:hover,
	.btn-finish:focus,
	.btn-finish:active {
		color: white;
		background: green;
	}
</style>