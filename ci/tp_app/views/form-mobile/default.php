<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Submission Form</title>

		<link rel="stylesheet" href="<?php echo lib_url('ext/bootstrap/css/bootstrap.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/font-awesome/css/font-awesome.min.css');?>" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="<?php echo lib_url('ext/jquery-ui/jquery-ui.min.css');?>" type="text/css" charset="utf-8"/>
		<!-- Custom Style -->
		<?php echo $style; ?>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

	</head>

	<body>
	<div class="container-fluid">
		<?php echo $form; ?>
	</div>

	<script type="text/javascript" src="<?php echo lib_url('ext/jquery/jquery-1.11.3.min.js'); ?>"></script>
	<script>
		var baseURL = "<?php echo base_url(); ?>";
		$(function(){
			try{
				<?php printDomreadyJS(); ?>
				console.log('DOM Ready JS Execution Complete');
			} catch(err) {
				console.log(err);
			}
		});
	</script>
	</body>
</html>