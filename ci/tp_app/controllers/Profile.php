<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends TP_Controller {

	function __construct()
	{
		parent::__construct('Profile');
	}

	public function index()
	{
		$title = 'Profile';
		$id = $this->psecurity->getUserLoggedIn();

		$section = $this->input->get('section');
		switch ($section) {
			case 'change-password':
				$btnAction = 'trackplus.profile.chPass('.$id.')';
				$data = array(
						'id'		=> $id,
						'btnAction'	=> $btnAction
					);
				echo $this->pagebuilder->renderPage('password', $title, $data, true);
				break;
			case 'edit':
				$btnAction = 'trackplus.profile.save('.$id.')';

				$adminQuery = $this->db->query('select * from '.TBL_ADMINS.' where id = '.$this->db->escape($id));
				if ($adminQuery->num_rows() > 0) {
					$admin = $adminQuery->row();
					$email = $admin->email;
				}

				$profileQuery = $this->db->query('select * from '.TBL_ADMIN_PROFILES.' where adminId = '.$this->db->escape($id));
				if($profileQuery->num_rows() > 0){
					$adminProfile = $profileQuery->row();
					$name = $adminProfile->name;
					$address = $adminProfile->address;
					$phone = $adminProfile->phone;
				}

				$data = array(
						'id'		=> $id,
						'email' 	=> $email,
						'name' 		=> $name,
						'address' 	=> $address,
						'phone' 	=> $phone,
						'btnAction'	=> $btnAction
					);

				echo $this->pagebuilder->renderPage('profile', $title, $data, true);
				break;
			default:
				redirect(site_url('profile?section=edit'));
				break;
		}
	}
}
