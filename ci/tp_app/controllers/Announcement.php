<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends TP_Controller {

	function __construct()
	{
		parent::__construct('Announcements');
	}

	public function index()
	{
		$title = 'Announcement';
		$js = $this->populateTableJS();
		addDomreadyJS($js);
		echo $this->pagebuilder->renderPage('announcement/main', $title, array(), true);
	}

	public function populateTableJS() {
		$companyId = $this->psecurity->getUserLoggedIn('company');
		$query = $this->db->query("
				SELECT A.id ID, A.title Title, A.content Content, IF(A.published = 1, 'Published', 'Unpublished') Status
				FROM ".TBL_ANNOUNCEMENTS." A
				WHERE A.companyId = ".$this->db->escape($companyId)." AND
					  A.deletedAt IS NULL
			");

		$fields = $query->list_fields();
		$columns = array();

		foreach ($fields as $field) {
			if ($field != "ID") {
				$columns[] = array("title" => $field);
			} else {
				$columns[] = array("title" => "No");
			}
		}
		$columns[] = array("title" => "Action");

		$data = array();

		if ($query->num_rows() > 0) {
			$announcements = $query->result();

			foreach ($announcements as $key => $announcement) {
				$announcement->No = "";
				$announcement->Action = '
					<div class="no-margin no-padding table-option-wrapper">
					<a href="'.base_url().'announcement/manage/edit/'.$announcement->ID.'" class="table-option border">Edit</a>
					<a class="table-option delete-row border" onclick="trackplus.announcements.delete('.$announcement->ID.', $(this))">Delete</a>
					</div>
				';

				$temp = array(
						$announcement->No,
						$announcement->Title,
						$announcement->Content,
						$announcement->Status,
						$announcement->Action
					);

				array_push($data, $temp);
			}
		}

		// Table JS
		$js = '
			var table = $("#announcement_table").DataTable({
				"data": '.json_encode($data).',
	        	"columns": '.json_encode($columns).',
	        	"columnDefs": [{
		            "searchable": false,
		            "orderable": false,
		            "targets": [0, 4]
		        },
		        {
		        	width: "40%",
		        	targets: 2
		        }],
		        "order": [[1, "asc"]],
	        	"scrollY": 200,
	        	"scrollX": true,
	        	"oLanguage": {
			        "sEmptyTable": "No Data Found"
			    }
			});

			table.on("order.dt search.dt", function()
			{
		        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
		            cell.innerHTML = i+1;
		        });
		    }
		    ).draw();
		';

		return $js;
	}

	public function manage($action, $id=0) {
		$title = 'Announcement';
		$atitle = '';
		$content = '';
		$published = 0;
		$btnAction = 'trackplus.announcements.save()';
		$btnActionPublishJS = '';
		$selectedProjects = array();

		// $projectQuery = $this->db->query('select * from '.TBL_PROJECTS.' where companyId = '.$this->db->escape( $this->psecurity->getUserLoggedIn('company')));
		// $projects = $projectQuery->result();

		if ($action == 'edit') {
			$btnAction = 'trackplus.announcements.save('.$id.')';

			$announcementQuery = $this->db->query('select * from '.TBL_ANNOUNCEMENTS.' where id = '.$this->db->escape($id));
			if ($announcementQuery->num_rows() > 0) {
				$announcement = $announcementQuery->row();

				$atitle = $announcement->title;
				$content = $announcement->content;
				$published = $announcement->published;
				// $selectedProjects = unserialize($announcement->projects);

				$btnActionPublishJS = '
					trackplus.announcements.publish = function(status) {
				        var post = "action=publish&id='.$id.'&status="+status;
			            trackplus.request.ajax_request("announcement", post);
				    };

				    $("a#btn_publish_announcement").on("click", function() {
				    	var text = $("a#btn_publish_announcement").text().trim();
				    	var status = 1;
						if (text.toLowerCase() == "unpublish") {
							status = 0;
						}
				    	trackplus.announcements.publish(status);
				    });
				';
			}
		}

		$js = "
			CKEDITOR.replace('announcementEditor');
		";

		$js .= $btnActionPublishJS;

		addDomreadyJS($js);

		$data = array(
				'action'	 		=> $action,
				'id'				=> $id,
				'title'				=> $atitle,
				'content' 			=> $content,
				'published'			=> $published,
				'btnAction'			=> $btnAction
			);

		echo $this->pagebuilder->renderPage('announcement/manage', $title, $data, true);
	}
}