<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends TP_Controller {

	function __construct()
	{
		parent::__construct('Admin');
	}

	public function index()
	{
		$title = "Admin";
		$js = $this->populateTableJS();
		addDomreadyJS($js);
		echo $this->pagebuilder->renderPage('admin/main', $title, array(), true);
	}

	public function manage($action, $id=0)
	{
		$title = 'Admin';
		$email = '';
		$name = '';
		$address  = '';
		$phone = '';
		$btnAction = 'trackplus.admin.add()';

		if ($action == 'edit') {
			$btnAction = 'trackplus.admin.edit('.$id.')';

			$marketingQuery = $this->db->query('select * from '.TBL_ADMINS.' where id = '.$this->db->escape($id));
			if ($marketingQuery->num_rows() > 0) {
				$marketing = $marketingQuery->row();
				$email = $marketing->email;
			}

			$profileQuery = $this->db->query('select * from '.TBL_ADMIN_PROFILES.' where adminId = '.$this->db->escape($id));
			if($profileQuery->num_rows() > 0){
				$marketingProfile = $profileQuery->row();

				$name = $marketingProfile->name;
				$address = $marketingProfile->address;
				$phone = $marketingProfile->phone;
			}
		}

		$data = array(
				'action' 	=> $action,
				'id'		=> $id,
				'email' 	=> $email,
				'name' 		=> $name,
				'address' 	=> $address,
				'phone' 	=> $phone,
				'btnAction'	=> $btnAction
			);

		echo $this->pagebuilder->renderPage('admin/manage', $title, $data, true);
	}

	public function populateTableJS() {
		$companyId = $this->psecurity->getUserLoggedIn('company');
		$query = $this->db->query("
				SELECT A.id ID, A.email Email, AP.name Name, AP.address Address, AP.Phone
				FROM ".TBL_ADMINS." A, ".TBL_ADMIN_PROFILES." AP
				WHERE A.id = AP.adminId AND
					  A.id <> ".$this->db->escape($this->psecurity->getUserLoggedIn())." AND
					  A.companyId = ".$this->db->escape($companyId)." AND
					  A.deletedAt IS NULL
			");

		$fields = $query->list_fields();
		$columns = array();

		foreach ($fields as $field) {
			if ($field != "ID") {
				$columns[] = array("title" => $field);
			} else {
				$columns[] = array("title" => "No");
			}
		}
		$columns[] = array("title" => "Action");

		$data = array();

		if ($query->num_rows() > 0) {
			$admins = $query->result();

			foreach ($admins as $key => $admin) {
				$admin->No = "";
				$admin->Action = '
					<div class="no-margin no-padding table-option-wrapper">
					<a href="'.base_url().'admin/manage/edit/'.$admin->ID.'" class="table-option border">Edit</a>
					<a class="table-option delete-row border" onclick="trackplus.admin.delete('.$admin->ID.', $(this))">Delete</a>
					</div>
				';
				$temp = array(
						$admin->No,
						$admin->Email,
						$admin->Name,
						$admin->Address,
						$admin->Phone,
						$admin->Action
					);

				array_push($data, $temp);
			}
		}

		// Table JS
		$js = '
			var table = $("#admin_table").DataTable({
				"data": '.json_encode($data).',
	        	"columns": '.json_encode($columns).',
	        	"columnDefs": [{
		            "searchable": false,
		            "orderable": false,
		            "targets": [0,5]
		        }],
		        "order": [[ 1, "asc" ]],
	        	"scrollY": 200,
	        	"scrollX": true,
	        	"oLanguage": {
			        "sEmptyTable": "No Data Found"
			    }
			});

			table.on("order.dt search.dt", function()
			{
		        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
		            cell.innerHTML = i+1;
		        });
		    }
		    ).draw();
		';

		return $js;
	}

}
