<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends TP_Controller {
	function __construct()
	{
		parent::__construct('Marketing');
	}

	public function index()
	{
		$title = "Marketing";
		//echo $this->PopulateTable();
		$js = $this->populateTableJS();
		addDomreadyJS($js);
		echo $this->pagebuilder->renderPage('marketing/main', $title, array(), true);
	}

	public function manage($action, $id=0){
		$title = 'Marketing';
		$email = '';
		$name = '';
		$address  = '';
		$phone = '';
		$btnAction = 'trackplus.marketing.add_marketing()';

		if ($action == 'edit') {
			$btnAction = 'trackplus.marketing.edit_marketing('.$id.')';

			$marketingQuery = $this->db->query('select * from '.TBL_MARKETINGS.' where id = '.$this->db->escape($id));
			if ($marketingQuery->num_rows() > 0) {
				$marketing = $marketingQuery->row();
				$email = $marketing->email;
			}

			$profileQuery = $this->db->query('select * from '.TBL_MARKETING_PROFILES.' where marketingId = '.$this->db->escape($id));
			if($profileQuery->num_rows() > 0){
				$marketingProfile = $profileQuery->row();

				$name = $marketingProfile->name;
				$address = $marketingProfile->address;
				$phone = $marketingProfile->phone;
			}
		}

		$data = array(
				"action" 	=> $action,
				"id"		=> $id,
				"email" 	=> $email,
				"name" 		=> $name,
				"address" 	=> $address,
				"phone" 	=> $phone,
				"btnAction"	=> $btnAction
			);

		echo $this->pagebuilder->renderPage('marketing/manage', $title, $data, true);
	}


/*=============================== test table aras =====================================================*/

	public function populateTableJS() {
		$companyId = $this->psecurity->getUserLoggedIn('company');
		$query = $this->db->query("
				SELECT M.id ID, M.email Email, MP.name Name, MP.address Address, MP.phone Phone
				FROM ".TBL_MARKETINGS." M, ".TBL_MARKETING_PROFILES." MP
				WHERE M.id = MP.marketingId AND
					  M.companyId = ".$this->db->escape($companyId)." AND
					  M.deletedAt IS NULL
			");

		$fields = $query->list_fields();
		$columns = array();

		foreach ($fields as $field) {
			if ($field != "ID") {
				$columns[] = array("title" => $field);
			} else {
				$columns[] = array("title" => "No");
			}
		}
		$columns[] = array("title" => "Action");

		$data = array();

		if ($query->num_rows() > 0) {
			$marketings = $query->result();

			foreach ($marketings as $key => $marketing) {
				$marketing->No = "";
				$marketing->Action = '
					<div class="no-margin no-padding table-option-wrapper">
					<a href="'.site_url('marketing/manage/edit/'.$marketing->ID).'" class="table-option border">Edit</a>
					<a class="table-option delete-row border" onclick="trackplus.marketing.delete_marketing('.$marketing->ID.', $(this))">Delete</a>
					</div>
				';
				$temp = array(
						$marketing->No,
						$marketing->Email,
						$marketing->Name,
						$marketing->Address,
						$marketing->Phone,
						$marketing->Action
					);

				array_push($data, $temp);
			}
		}

		// Table JS
		$js = '
			var table = $("#marketing_table").DataTable({
				"data": '.json_encode($data).',
	        	"columns": '.json_encode($columns).',
	        	"columnDefs": [{
		            "searchable": false,
		            "orderable": false,
		            "targets": 0
		        }],
		        "order": [[ 1, "asc" ]],
	        	"scrollY": 200,
	        	"scrollX": true,
	        	"language": {
			        "emptyTable": "No Data Found"
			    }
			});

			table.on("order.dt search.dt", function()
			{
		        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
		            cell.innerHTML = i+1;
		        });
		    }
		    ).draw();
		';

		return $js;
	}

}