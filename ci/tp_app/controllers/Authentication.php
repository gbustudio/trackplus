<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends TP_Controller {

	function __construct()
	{
		parent::__construct('Authentication');
	}

	public function login()
	{
		if ($this->psecurity->isUserLoggedIn()) {
			redirect(site_url());
		}

		$title = 'Login';
		$js = '
			$("form").on("keypress", function(e) {
				if (e.which == 13) {
					trackplus.authentication.login($(this).find(".btn"));
				}
			});
		';
		addDomReadyJS($js);
		echo $this->pagebuilder->renderPage('login', $title);
	}

	public function logout()
	{
		$this->psecurity->logout();
		redirect(base_url().'authentication/login');
	}

}
