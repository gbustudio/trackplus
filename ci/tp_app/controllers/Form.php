<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends TP_Controller {
	function __construct()
	{
		parent::__construct('Form');
		$this->load->library(array('form_factory', 'form_item_factory', 'form_builder'));
	}

	public function index()
	{
		$title = "Form";
		$js = $this->populateTableJS();
		addDomreadyJS($js);

		echo $this->pagebuilder->renderPage('form/main', $title, array(), true);
	}

	public function manage($action, $id=0)
	{
		$title = "Form";
		$btnAction = 'trackplus.form.add_form()';
		$marketings = array();
		$name = "";
		$desc = "";
		$havePage = true;

		// $marketings = $this->form_factory->getAvailableMarketing($id);

		if ($action == "edit") {
			$btnAction = 'trackplus.form.edit_form('.$id.')';
			$form = $this->form_factory->getOne($id);
			if ($form) {
				$name = $form->name;
				$desc = $form->description;
				$havePage = $form->havePage;
			}
		}

		$data = array(
				"formId"	 	=> $id,
				"name" 			=> $name,
				"desc" 			=> $desc,
				"havePage" 		=> $havePage,
				"marketings" 	=> $marketings,
				"btnAction"		=> $btnAction,
				"action" 		=> $action,
			);

		echo $this->pagebuilder->renderPage('form/manage', $title, $data, true);
	}

	public function builder($id = 0)
	{
		$title = "Form";
		$form = $this->form_factory->getOne($id);

		if ($form == false) {
			redirect(base_url('dashboard'));
		}

		$pageHtml = '';
		$reorderItemsHtml = '';
		$btnAction = 'trackplus.form.publish('.$id.')';
		if ($form->havePage) {
			$pageHtml = $this->form_builder->renderPageList($id);
		} else {
			$reorderItemsHtml = $this->form_builder->renderReorderItemsHtml($id, 0);
		}

		$optsHtml = $this->form_builder->renderFormOptionButton($id);
		$itemsHtml = $this->form_builder->renderItemsHtml($id, 0);

		$data = array(
				'formId' 			=> $id,
				'btnAction'			=> $btnAction,
				'name'				=> $form->name,
				'havePage'			=> $form->havePage,
				'pageHtml'			=> $pageHtml,
				'optsHtml'			=> $optsHtml,
				'itemsHtml'			=> $itemsHtml,
				'reorderItemsHtml'	=> $reorderItemsHtml
			);

		echo $this->pagebuilder->renderPage('form/builder', $title, $data, true);
	}

	public function populateTableJS()
	{
		$companyId = $this->psecurity->getUserLoggedIn('company');
		$query = $this->db->query('
				SELECT F.id ID, F.name Name, F.description Description, F.havePage HavePage, F.published Published, F.published Status
				FROM '.TBL_FORMS.' F
				WHERE F.companyId = '.$this->db->escape($companyId).' AND
					  F.deletedAt IS NULL
			');

		$fields = $query->list_fields();
		$columns = array();

		foreach ($fields as $field) {
			switch ($field) {
				case "ID":
					$columns[] = array("title" => "No");
					break;
				case "Published";
					break;
				case "HavePage":
					$columns[] = array("title" => "Have Page");
					break;
				default:
					$columns[] = array("title" => $field);
					break;
			}
		}
		$columns[] = array("title" => "Action");

		$data = array();

		if ($query->num_rows() > 0) {
			$forms = $query->result();

			foreach ($forms as $key => $form) {
				$form->No = "";
				$btnAction = '<div class="no-margin no-padding table-option-wrapper">';
				$btnAction .= '<a href="'.site_url('form/manage/edit/'.$form->ID).'" class="table-option border">Edit</a>&nbsp;';
				if ($form->Published == 1) {
					$btnAction .= '<a class="table-option border" href="'.site_url('form/submission/'.$form->ID).'" >Submission</a>&nbsp;';
				} else {
					$btnAction .= '<a class="table-option border" href="'.site_url('form/builder/'.$form->ID).'" >Form</a>&nbsp;';
				}
				$btnAction .= '<a class="table-option delete-row border" onclick="trackplus.form.delete('.$form->ID.', $(this))">Delete</a>';
				$btnAction .= '</div>';

				$form->Action = $btnAction;

				$temp = array(
						$form->No,
						$form->Name,
						$form->Description,
						$form->HavePage ? "Yes" : "No",
						$form->Status ? "Published" : "Unpublished",
						$form->Action
					);

				array_push($data, $temp);
			}
		}

		// Table JS
		$js = '
			var table = $("#project_table").DataTable({
				"data": '.json_encode($data).',
	        	"columns": '.json_encode($columns).',
	        	"columnDefs": [{
			            "searchable": false,
			            "orderable": false,
			            "targets": [0, 5],
			        },
			        {
			            className: "dt-center",
			            "targets": [3, 4, 5],
			        },
			        {
			            className: "dt-head-center",
			            "targets": [0, 1, 2, 3, 4, 5],
			        },
					{
			            "width": "5%",
			            "targets": 0
			        },
			        {
			            "width": "20%",
			            "targets": 5
			        }],
		        "order": [[1, "asc"]],
	        	"scrollY": 200,
	        	"scrollX": true,
	        	"oLanguage": {
			        "sEmptyTable": "No Data Found"
			    }
			});

			table.on("order.dt search.dt", function()
			{
		        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
		            cell.innerHTML = i+1;
		        });
		    }
		    ).draw();
		';
		return $js;
	}

	public function submission($projectId = 0) {
		$title = "Project";
		$data = [];
		$project = $this->form_factory->getOne($projectId);
		if ($project) {
			$data['projectId'] = $projectId;
			$data['projectName'] = $project->name;
			addDomreadyJS($this->populateSubmissionTableJS($projectId));
		}
		echo $this->pagebuilder->renderPage('form/submission', $title, $data, true);
	}

	public function populateSubmissionTableJS($projectId) {
		$project = $this->form_factory->getOne($projectId);
		$items = $this->form_item_factory->getAll($projectId, $project->havePage);
		$formSubmissions = $this->form_item_factory->getSubmissions($projectId);
		$columns = [
			array("title" => "No"),
			array("title" => "Date/Time"),
			array("title" => "Marketing")
		];

		$itemList = [];
		if ($items) {
			foreach ($items as $item) {
				// Fill column header using item name
				$columns[] = array("title" => $item->name);
				// Save all itemId on itemList array to be used on getting submission data in item order and checking if submission data for an item is available or not.
				$itemList[] = $item->id;
			}
		}

		$data = [];
		if ($formSubmissions) {
			foreach ($formSubmissions as $formSubmission) {
				// Fill first 3 column (No, Date, and Marketing)
				$temp = [
					'',
					$formSubmission->Date,
					$formSubmission->Marketing
				];

				// Fill the rest of column using submission data
				$pages = unserialize($formSubmission->Submission);
				if ($pages) {
					$submissions = [];
					foreach ($pages as $pageIndex => $page) {
						// Merge all items from all page
						$submissions += $page;
					}

					/**
					 * Show submission data based on itemList
					 *
					 * This step is needed because when user submitting data,
					 * some of item may not have any data and to prevent missing column error on displaying the submission data,
					 * compare the data from itemList and give empty value for the empty column.
					 **/
					foreach ($itemList as $item) {
						// Checking if submission data for item 'x' is available or not.
						if (isset($submissions[$item]) && $submissions[$item] != '') {
							if (is_array($submissions[$item])) {
								// If submission data have more than one value
								$sData = '';
								foreach ($submissions[$item] as $idx => $value) {
									if ($idx > 0) {
										// Add new line before adding the next value
										$sData .= '<br/>';
									}
									$sData .= $value;
								}
							} else {
								// If submission data only have one value
								$sData = $submissions[$item];
							}
							$temp[] = $sData;
						} else {
							// If submission data isn't available or not exists in the array
							$temp[] = '-';
						}
					}
				}
				array_push($data, $temp);
			}

			$js = '
				var table = $("#submission_table").DataTable({
					"data"		: '.json_encode($data).',
		        	"columns"	: '.json_encode($columns).',
		        	"columnDefs":
		        	[{
			            "searchable": false,
			            "orderable": false,
			            "targets": [0],
			        },
			        {
			        	width: "5%",
			        	targets: [0,1]
			        },
			        {
			        	width: "10%",
			        	targets: 2
			        }],
				    "order"		: [[1, "asc"]],
		        	"oLanguage"	:
		        	{
				        "sEmptyTable": "No Data Found"
				    },
				    "scrollX": true,
				});

				table.on("order.dt search.dt", function()
				{
			        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
			            cell.innerHTML = i+1;
			        });
			    }
			    ).draw();
			';

		} else {
			$js = '
				$("#submission_table").html("No Submission Available");
				$("#buttonWrapper").remove();
			';
		}
		return $js;
	}

	public function export_csv($formId)
	{
		$form = $this->form_factory->getOne($formId);

		header("Content-type: application/csv");
	    header("Content-Disposition: attachment; filename=\"".url_title($form->name)."-submissions-".url_title(date('d M Y')).".csv\"");
	    header("Pragma: no-cache");
	    header("Expires: 0");
	    $handle = fopen('php://output', 'w');

		$items = $this->form_factory->getAll($formId, $form->havePage);
		$formSubmissions = $this->form_factory->getSubmissions($formId);

		$title = ['Date', 'Marketing'];
		$itemList = [];
		if ($items) {
			foreach ($items as $item) {
				$title[] = $item->name;
				$itemList[] = $item->id;
			}
		}
		fputcsv($handle, $title);

		if ($formSubmissions) {
			foreach ($formSubmissions as $formSubmission) {
				// Fill first 3 column (No, Date, and Marketing)
				$temp = [
					$formSubmission->Date,
					$formSubmission->Marketing
				];

				// Fill the rest of column using submission data
				$pages = unserialize($formSubmission->Submission);
				if ($pages) {
					$submissions = [];
					foreach ($pages as $pageIndex => $page) {
						// Merge all items from all page
						$submissions += $page;
					}

					foreach ($itemList as $item) {
						if (isset($submissions[$item]) && $submissions[$item] != '') {
							if (is_array($submissions[$item])) {
								$sData = '';
								foreach ($submissions[$item] as $idx => $value) {
									if ($idx > 0) {
										$sData .= ' & ';
									}
									$sData .= $value;
								}
							} else {
								$sData = $submissions[$item];
							}
							$temp[] = $sData;
						} else {
							$temp[] = '-';
						}
					}
				}
				fputcsv($handle, $temp);
			}
		}

        fclose($handle);
	}

}