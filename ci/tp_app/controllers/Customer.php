<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends TP_Controller {

	function __construct()
	{
		parent::__construct('Customer');
	}

	public function index()
	{
		$title = "Customer";
		$js = $this->populateTableJS();
		addDomreadyJS($js);
		echo $this->pagebuilder->renderPage('customer/main', $title, array(), true);
	}

	public function manage($action, $id=0)
	{
		if ($action == 'edit' or $action == 'new') {
			$title = 'Customer';
			$name = '';
			$address = '';
			$phone  = '';
			$fax = '';
			$cpName = '';
			$cpEmail  = '';
			$cpPhone = '';
			$btnAction = 'trackplus.customer.add()';

			if ($action == 'edit') {
				$btnAction = 'trackplus.customer.edit('.$id.')';

				$customerQuery = $this->db->query('select * from '.TBL_CUSTOMERS.' where id = '.$this->db->escape($id));
				if ($customerQuery->num_rows() > 0) {
					$customer = $customerQuery->row();
					$name = $customer->name;
					$address = $customer->address;
					$phone  = $customer->phone;
					$fax = $customer->fax;
					$cpName = $customer->cpName;
					$cpEmail  = $customer->cpEmail;
					$cpPhone = $customer->cpPhone;
				}
			}

			$data = array(
					'action'    => $action,
					'id'        => $id,
					'name'      => $name,
					'address'   => $address,
					'phone'     => $phone,
					'fax'       => $fax,
					'cpName'    => $cpName,
					'cpEmail'   => $cpEmail,
					'cpPhone'   => $cpPhone,
					'btnAction' => $btnAction
				);

			echo $this->pagebuilder->renderPage('customer/manage', $title, $data, true);
		} else {
			show_404();
		}
	}

	public function populateTableJS()
	{
		$query = $this->db->query("
				SELECT id ID, name Name, address Address, cpName 'Contact Person' , cpEmail 'CP Email', cpPhone 'CP Phone'
				FROM ".TBL_CUSTOMERS."
				WHERE deletedAt IS NULL
			");

		$fields = $query->list_fields();
		$columns = array();

		foreach ($fields as $field) {
			if ($field != "ID") {
				$columns[] = array("title" => $field);
			} else {
				$columns[] = array("title" => "No");
			}
		}
		$columns[] = array("title" => "Action");

		$data = array();

		if ($query->num_rows() > 0) {
			$customers = $query->result();
			// var_dump($customers); exit();
			foreach ($customers as $key => $customer) {
				$customer->No = "";
				$customer->Action = '
					<div class="no-margin no-padding table-option-wrapper">
					<a href="'.base_url().'customer/manage/edit/'.$customer->ID.'" class="table-option border">Edit</a>
					<a class="table-option delete-row border" onclick="trackplus.customer.delete('.$customer->ID.', $(this))">Delete</a>
					</div>
				';
				$temp = array(
						$customer->No,
						$customer->Name,
						$customer->Address,
						$customer->{'Contact Person'},
						$customer->{'CP Email'},
						$customer->{'CP Phone'},
						$customer->Action
					);

				array_push($data, $temp);
			}
		}

		// Table JS
		$js = '
			var table = $("#customer_table").DataTable({
				"data": '.json_encode($data).',
	        	"columns": '.json_encode($columns).',
	        	"columnDefs": [{
		            "searchable": false,
		            "orderable": false,
		            "targets": [0,6]
		        }],
		        "order": [[ 1, "asc" ]],
	        	"scrollY": 200,
	        	"scrollX": true,
	        	"oLanguage": {
			        "sEmptyTable": "No Data Found"
			    }
			});

			table.on("order.dt search.dt", function()
			{
		        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
		            cell.innerHTML = i+1;
		        });
		    }
		    ).draw();
		';

		return $js;
	}

}