<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends TP_Controller {

	function __construct()
	{
		parent::__construct('Request');
	}

	private function build_json($status, $output=array())
	{
		return json_encode(array_merge(array('status'=>$status), $output));
	}

	public function user()
	{
		$action = $this->input->post('action');
		switch ($action) {
			case 'login':
				$status = 'OK';
				$response = array();
				// Admin Login
				$email = trim($this->input->post('userEmail'));
				$password = $this->input->post('userPassword');

				if ($email == false || $email == '') {
					$status = 'Failed';
					$response['errors'][] = "Email Dibutuhkan!";
				}
				if ($password == false || $password == '') {
					$status = 'Failed';
					$response['errors'][] = "Password Dibutuhkan!";
				}

				if ($status == 'OK') {
					// Check user credentials
					$isAuthenticated = $this->psecurity->authenticateAdmin($email, $password);
					if ($isAuthenticated) {
						// Redirect to Dashboard
						$response['js'] = '
							window.location.replace("'.site_url().'");
						';
					} else {
						// Show Error Message
						$status = 'Failed';
						$response['errors'][] = "Email/Password Salah!";
					}

					$response['data'] = array('Password' => $password);
				}

				echo $this->build_json($status, $response);
				break;
			case 'register':
				break;
			case 'manage_rules':
				break;
		}
	}

	public function profile() {
		$status = 'OK';
		$response = [];
		$action = $this->input->post('action');
		switch ($action) {
			case 'save':
				$id = $this->input->post('id');
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));

				if ($id == false) {
					$status = 'Failed';
					$response['errors'][] = 'Identifier Required!';
				}

				if ($status == 'OK') {
					$data = array(
						'name' 		=> $name,
						'address' 	=> $address,
						'phone' 	=> $phone
					);

					if ($this->db->update(TBL_ADMIN_PROFILES, $data, array('adminId' => $id))) {
						$response['success'][] = 'Profile successfully updated';
						$screenName = $this->admin_factory->getScreenName($id);
						$response['js'] = "
							$('#screenName').html('".$screenName."');
						";
					}
				}
				break;
			case 'chpass':
				$id = $this->input->post('id');
				$current = trim($this->input->post('current'));
				$new = trim($this->input->post('new'));
				$rnew = trim($this->input->post('retypeNew'));

				if ($id == false) {
					$status = 'Failed';
					$response['errors'][] = 'Identifier Required!';
				}
				if ($current == false || $current == '') {
					$status = 'Failed';
					$response['errors'][] = 'Current Password Required!';
				} else {
					if (!$this->psecurity->checkCurrentPassword($id, $current)) {
						$status = 'Failed';
						$response['errors'][] = 'Wrong Current Password!';
					}
				}
				if ($new == false || $new == '') {
					$status = 'Failed';
					$response['errors'][] = 'New Password Required!';
				} else {
					if (strlen($new) < 6) {
						$status = 'Failed';
						$response['errors'][] = 'Password length MUST BE >= 6';
					} else {
						if ($new != $rnew) {
							$status = 'Failed';
							$response['errors'][] = 'New Password Does Not Match!';
						}
					}
				}

				if ($status == 'OK') {
					$md5Passw = md5($new);
					$pwdLen = strlen($new);
					$encPass = $this->psecurity->encryptPassword($md5Passw, $pwdLen);
					$data = array(
						'password'	=> $encPass,
						'pwdLen' 	=> $pwdLen,
					);

					if ($this->db->update(TBL_ADMINS, $data, array('id' => $id))) {
						$response['success'][] = 'Password successfully updated';
						$response['js'] = "
							$('#current').val('');
							$('#new').val('');
							$('#retypeNew').val('');
						";
					}
				}
				break;
			default:
				$status = 'Failed';
				$response['errors'][] = 'Illegal Action!';
				break;
		}
		echo $this->build_json($status, $response);
	}

	public function admin()
	{
		$action = $this->input->post('action');
		switch ($action) {
			case 'add':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$email = trim($this->input->post('email'));
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));

				if ($email == false || $email == '') {
					$status = 'Failed';
					$response['errors'][] = 'Email harus diisi!';
				} else {
					$dupEmailAdmin = $this->db->query('
							SELECT *
							FROM '.TBL_ADMINS.'
							WHERE lower(email) = '.$this->db->escape(strtolower($email)).'
						');
					$dupEmailMarketing = $this->db->query('
							SELECT *
							FROM '.TBL_MARKETINGS.'
							WHERE lower(email) = '.$this->db->escape(strtolower($email)).'
						');
					if ($dupEmailAdmin->num_rows() > 0 || $dupEmailMarketing->num_rows() > 0) {
						$status = 'Failed';
						$response['errors'][] = 'Email Sudah Terdaftar!';
					}
				}

				if ($status == 'OK') {
					$password = random_string("alnum", 16);
					$md5Password = md5($password);
					$pwdLen = strlen($password);

					$encPass = $this->psecurity->encryptPassword($md5Password, $pwdLen);

					$data = array(
						'companyId' => $this->psecurity->getUserLoggedIn('company'),
						'email' 	=> $email,
						'password' 	=> $encPass,
						'pwdLen' 	=> $pwdLen,
						'updatedAt' => $now,
						'createdAt' => $now
					);

					if ($this->db->insert(TBL_ADMINS, $data)) {
						$adminId = $this->db->insert_id();
						$profile = array(
							'name' 		=> $name,
							'address' 	=> $address,
							'phone' 	=> $phone
						);
						$this->db->update(TBL_ADMIN_PROFILES, $profile, array('adminId' => $adminId));

						$this->load->library("email_factory");
						if ($this->email_factory->sendNewRegistration($email, $password)) {
							$response['js'] = '
								window.location = "'.site_url('admin').'";
							';
						}
					}
				}

				echo $this->build_json($status, $response);
				break;
			case "edit":
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$id = $this->input->post('id');
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));

				$data = array(
					'adminId' 	=> $id,
					'name' 		=> $name,
					'address' 	=> $address,
					'phone' 	=> $phone
				);

				if ($this->db->update(TBL_ADMIN_PROFILES, $data, array('adminId' => $id))) {
					$response['js'] = "
						window.location = '".site_url('admin')."';
					";
				}

				echo $this->build_json($status, $response);
				break;
			case 'delete':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$adminId = $this->input->post('adminId');
				if ($adminId == false || $adminId == "") {
					$status = 'Failed';
					$response['errors'][] = 'Admin tidak ditemukan';
				}

				if ($status == 'OK') {
					if ($this->db->query('update '.TBL_ADMINS.' set deletedAt = '.$this->db->escape($now).' WHERE id = '.$this->db->escape($adminId))) {
						if ($this->db->query('update '.TBL_ADMIN_PROFILES.' set deletedAt = '.$this->db->escape($now).' WHERE adminId = '.$this->db->escape($adminId))) {
							$response['js'] = "
								var table = $('#admin_table').DataTable();
								table.row('.selected').remove().draw(false);
							";
						}
					}
				}

				echo $this->build_json($status, $response);
				break;
		}
	}

	public function customer()
	{
		$action = $this->input->post('action');
		switch ($action) {
			case 'add':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$companyId = $this->psecurity->getUserLoggedIn('company');
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));
				$fax = trim($this->input->post('fax'));
				$cpName = trim($this->input->post('cpName'));
				$cpEmail = trim($this->input->post('cpEmail'));
				$cpPhone = trim($this->input->post('cpPhone'));

				if ($name == false || $name == '') {
					$status = 'Failed';
					$response['errors'][] = 'Nama harus diisi!';
				} else {
					$dupCustomer = $this->db->query('
							SELECT *
							FROM '.TBL_CUSTOMERS.'
							WHERE lower(name) = '.$this->db->escape(strtolower($name)).' AND
								  lower(address) = '.$this->db->escape(strtolower($address)).' AND
								  companyId = '.$this->db->escape($companyId).'
						');
					if ($dupCustomer->num_rows() > 0) {
						$status = 'Failed';
						$response['errors'][] = 'Customer Sudah Terdaftar!';
					}
				}
				if ($phone == false || $phone == '') {
					$status = 'Failed';
					$response['errors'][] = 'Nomor Telepon harus diisi!';
				}
				if ($cpEmail != false && $cpEmail != '') {
					if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					    $status = 'Failed';
						$response['errors'][] = 'Penulisan Email Tidak Valid!';
					}
				}

				if ($status == 'OK') {
					$data = array(
						'companyId' => $companyId,
						'name'      => $name,
						'address'   => $address,
						'phone'     => $phone,
						'fax'       => $fax,
						'cpName'    => $cpName,
						'cpEmail'   => $cpEmail,
						'cpPhone'   => $cpPhone,
						'updatedAt' => $now,
						'createdAt' => $now
					);

					if($this->db->insert(TBL_CUSTOMERS, $data)){
						$response['js'] = "
							window.location = '".site_url('customer')."';
						";
					}
				}
				echo $this->build_json($status, $response);
				break;
			case 'edit':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$companyId = $this->psecurity->getUserLoggedIn('company');
				$id = $this->input->post('id');
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));
				$fax = trim($this->input->post('fax'));
				$cpName = trim($this->input->post('cpName'));
				$cpEmail = trim($this->input->post('cpEmail'));
				$cpPhone = trim($this->input->post('cpPhone'));

				if ($name == false || $name == '') {
					$status = 'Failed';
					$response['errors'][] = 'Nama harus diisi!';
				} else {
					$dupCustomer = $this->db->query('
							SELECT *
							FROM '.TBL_CUSTOMERS.'
							WHERE lower(name) = '.$this->db->escape(strtolower($name)).' AND
								  lower(address) = '.$this->db->escape(strtolower($address)).' AND
								  companyId = '.$this->db->escape($companyId).' AND
								  id <> '.$this->db->escape($id).'
						');
					if ($dupCustomer->num_rows() > 0) {
						$status = 'Failed';
						$response['errors'][] = 'Customer Sudah Terdaftar!';
					}
				}
				if ($phone == false || $phone == '') {
					$status = 'Failed';
					$response['errors'][] = 'Nomor Telepon harus diisi!';
				}
				if ($cpEmail != false && $cpEmail != '') {
					if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					    $status = 'Failed';
						$response['errors'][] = 'Penulisan Email Tidak Valid!';
					}
				}

				if ($status == 'OK') {
					$data = array(
						'name'      => $name,
						'address'   => $address,
						'phone'     => $phone,
						'fax'       => $fax,
						'cpName'    => $cpName,
						'cpEmail'   => $cpEmail,
						'cpPhone'   => $cpPhone
					);

					if ($this->db->update(TBL_CUSTOMERS, $data, array('id' => $id))) {
						$response['js'] = "
							window.location = '".site_url('customer')."';
						";
					}
				}

				echo $this->build_json($status, $response);
				break;
			case 'delete':
				$customerId = $this->input->post('customerId');
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				if ($customerId == false || $customerId == '') {
					$status = 'Failed';
					$response['errors'][] = 'ID customer tidak ditemukan';
				}

				if ($status == 'OK') {
					if ($this->db->query('update '.TBL_CUSTOMERS.' set deletedAt = '.$this->db->escape($now).' WHERE id = '.$this->db->escape($customerId))) {
						$response['js'] = "
								var table = $('#customer_table').DataTable();
								table.row('.selected').remove().draw(false);
							";
					}
				}

				echo $this->build_json($status, $response);
				break;
		}
	}

	public function announcement()
	{
		$action = $this->input->post('action');
		$status = "OK";
		$response = array();
		$now = getTimestamp(time());

		switch ($action) {
			case 'add':
				$title = trim($this->input->post('title'));
				$content = $this->input->post('content');

				if ($title == false || $title == "") {
					$status = 'Failed';
					$response['errors'][] = "Title required!";
				}

				if ($content == false || $content == "") {
					$status = 'Failed';
					$response['errors'][] = "Content required!";
				}

				if ($status == "OK") {
					$tasks = $this->input->post('tasks');
					if (!$tasks) {
						$tasks = array();
					}

					$data = array(
						'companyId' => $this->psecurity->getUserLoggedIn('company'),
						'title' 	=> $title,
						'content' 	=> $content,
						'published'	=> 0,
						'tasks'		=> serialize($tasks),
						'updatedAt' => $now,
						'createdAt' => $now
					);

					$id = $this->db->insert(TBL_ANNOUNCEMENTS, $data);

					if ($id != false) {
						$response['success'][] = 'Announcement successfully saved';
						$response['js'] = '
							$("a#btn_publish_announcement").removeClass("disabled");

							trackplus.announcements.publish = function() {
						        if (typeof(id) != "undefined") {
						            var post = "action=publish&id='.$id.'";
						            trackplus.request.ajax_request("announcement", post);
						        }
						    };

							if (!$("a#btn_publish_announcement").hasClass("disabled")) {
								$("a#btn_publish_announcement").on("click", function() {
							    	trackplus.announcements.publish();
							    });
							}
						';
					}
				}
				break;
			case 'edit':
				$id = $this->input->post('id');
				$title = trim($this->input->post('title'));
				$content = $this->input->post('content');

				if ($id == false) {
					$status = 'Failed';
					$response['errors'][] = "ID dibutuhkan!";
				}

				if ($title == false || $title == "") {
					$status = 'Failed';
					$response['errors'][] = "Title dibutuhkan!";
				}

				if ($content == false) {
					$status = 'Failed';
					$response['errors'][] = "Content dibutuhkan!";
				}

				if ($status == "OK") {
					$projects = $this->input->post('projects');
					if (!$projects) {
						$projects = array();
					}

					$response['projects'] = $projects;
					$where = array('id' => $id);
					$data = array(
						'title' 	=> $title,
						'content' 	=> $content,
						// 'projects' 	=> serialize($projects),
						'updatedAt' => $now
					);

					if ($this->db->update(TBL_ANNOUNCEMENTS, $data, $where)) {
						$response['success'][] = 'Announcement successfully saved';
					}
				}
				break;
			case 'publish':
				$id = $this->input->post('id');
				$isPublished = $this->input->post('status');

				if ($id == false) {
					$status = 'Failed';
					$response['errors'][] = "ID dibutuhkan!";
				}
				if ($isPublished === false) {
					$status = 'Failed';
					$response['errors'][] = "Status dibutuhkan!";
				}

				if ($status == "OK") {
					// $isPublished = $isPublished + (($isPublished * 2) - 1);
					$where = array('id' => $id);
					$data = array(
						'published' => $isPublished,
						'updatedAt' => $now
					);

					if ($this->db->update(TBL_ANNOUNCEMENTS, $data, $where)) {
						$response['publish'] = $isPublished;
						$response['js'] = '
							window.location = "'.site_url('announcement').'";
						';
					}
				}
				break;
			case 'delete':
				$id = $this->input->post('id');

				if ($id == false) {
					$status = 'Failed';
					$response['errors'][] = "ID dibutuhkan!";
				}

				if ($status == "OK") {
					// $isPublished = $isPublished + (($isPublished * 2) - 1);
					$where = array('id' => $id);
					$data = array(
						'deletedAt' => $now
					);

					if ($this->db->update(TBL_ANNOUNCEMENTS, $data, $where)) {
						$response['js'] = "
							var table = $('#announcement_table').DataTable();
							table.row('.selected').remove().draw( false );
						";
					}
				}
				break;
			default:
				$status = "Failed";
				$response['errors'][] = "Illegal Action!";
		}

		echo $this->build_json($status, $response);
	}

	public function marketing()
	{
		$action = $this->input->post('action');
		switch ($action) {
			case 'add_new':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$email = trim($this->input->post('email'));
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));

				if ($email == false || $email == '') {
					$status = 'Failed';
					$response['errors'][] = 'Email dibutuhkan!';
				} else {
					$dupEmailAdmin = $this->db->query('
							SELECT *
							FROM '.TBL_ADMINS.'
							WHERE lower(email) = '.$this->db->escape(strtolower($email)).'
						');
					$dupEmailMarketing = $this->db->query('
							SELECT *
							FROM '.TBL_MARKETINGS.'
							WHERE lower(email) = '.$this->db->escape(strtolower($email)).'
						');
					if ($dupEmailAdmin->num_rows() > 0 || $dupEmailMarketing->num_rows() > 0) {
						$status = 'Failed';
						$response['errors'][] = 'Email Sudah Terdaftar!';
					}
				}
				if ($name == false || $name == '') {
					$status = 'Failed';
					$response['errors'][] = 'Nama dibutuhkan!';
				}

				if ($status == 'OK') {
					$password = random_string("alnum", 16);
					$md5Password = md5($password);
					$pwdLen = strlen($password);

					$encPass = $this->psecurity->encryptPassword($md5Password, $pwdLen);

					$data = array(
						'email' 	=> $email,
						'companyId' => $this->psecurity->getUserLoggedIn('company'),
						'password' 	=> $encPass,
						'pwdLen' 	=> $pwdLen,
						'updatedAt' => $now,
						'createdAt' => $now
					);

					if ($this->db->insert(TBL_MARKETINGS, $data)) {
						$marketingId = $this->db->insert_id();
						$profile = array(
							'name' 		=> $name,
							'address' 	=> $address,
							'phone' 	=> $phone,
						);
						$this->db->update(TBL_MARKETING_PROFILES, $profile, array('marketingId' => $marketingId));

						$this->load->library("email_factory");
						if ($this->email_factory->sendNewRegistration($email, $password)) {
							$response['js'] = '
								window.location = "'.site_url('marketing').'";
							';
						}
					}
				}

				echo $this->build_json($status, $response);
				break;
			case 'delete':
				$marketingId = $this->input->post('marketingId');
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				if ($marketingId == false || $marketingId == '') {
					$status = 'Failed';
					$response['errors'][] = 'id marketing tidak ditemukan';
				}

				if ($status == 'OK') {
					if ($this->db->query('update '.TBL_MARKETINGS.' set deletedAt = '.$this->db->escape($now).' WHERE id = '.$this->db->escape($marketingId))) {
						if ($this->db->query('update '.TBL_MARKETING_PROFILES.' set deletedAt = '.$this->db->escape($now).' WHERE marketingId = '.$this->db->escape($marketingId))) {
							$response['js'] = "
								var table = $('#marketing_table').DataTable();
								table.row('.selected').remove().draw( false );
							";
						}
					}
				}

				echo $this->build_json($status, $response);
				break;
			case 'edit':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$id = $this->input->post('id');
				$name = trim($this->input->post('name'));
				$address = trim($this->input->post('address'));
				$phone = trim($this->input->post('phone'));

				if ($name == false || $name == '') {
					$status = 'Failed';
					$response['errors'][] = 'Nama dibutuhkan!';
				}

				if ($status == 'OK') {
					$data = array(
						'marketingId' => $id,
						'name' => $name,
						'address' => $address,
						'phone' => $phone,
						'updatedAt' => $now,
						'createdAt' => $now,
					);

					//$isUpdate = $this->db->query('select * from '.TBL_MARKETING_PROFILES.' where marketingId = '.$this->db->escape($id).'');
					//if ($isUpdate->num_rows() > 0) {
					if ($this->db->update(TBL_MARKETING_PROFILES, $data, array('marketingId' => $id))) {
						$response['js'] = "
							window.location = '".site_url('marketing')."';
						";
					}
				}

				echo $this->build_json($status, $response);
				break;
		}
	}

	public function form()
	{
		$this->load->library(['form_factory', 'page_factory', 'form_item_factory']);
		$action = $this->input->post('action');

		switch ($action) {
			case 'add_new':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());
				$name = trim($this->input->post('name'));
				$description = trim($this->input->post('description'));
				$havePage = $this->input->post('havePage');
				$marketings = $this->input->post('marketings');

				if ($name == false || $name == '') {
					$status = 'Failed';
					$response['errors'][] = 'Form Name Required!';
				} else {
					$dupName = $this->db->query('
						SELECT *
						FROM '.TBL_FORMS.'
						WHERE lower(name) = '.$this->db->escape(strtolower($name)).' AND
							  companyId = '.$this->psecurity->getUserLoggedIn('company').'
					');
					if ($dupName->num_rows() > 0) {
						$status = 'Failed';
						$response['errors'][] = 'Nama Sudah Terdaftar!';
					}
				}
				if ($havePage === false) {
					$status = 'Failed';
					$response['errors'][] = 'Have Page Required!';
				}
				if ($marketings == false) {
					$marketings = [];
				}

				if ($status == 'OK') {
					$slug = url_title(strtolower($name));
					$data = array(
						'companyId' 	=> $this->psecurity->getUserLoggedIn('company'),
						'name' 			=> $name,
						'description'	=> $description,
						'havePage'		=> $havePage,
						'slug' 			=> $slug,
						'updatedAt' 	=> $now,
						'createdAt' 	=> $now
					);

					if ($this->db->insert(TBL_FORMS, $data)) {
						$formId = $this->db->insert_id();
						foreach ($marketings as $marketingId) {
							$this->db->update(TBL_MARKETINGS, array('formId' => $formId), array('id' => $marketingId));
						}
					}

					$response['js'] = '
						window.location = "'.site_url('form/builder/'.$formId).'";
					';
				}

				echo $this->build_json($status, $response);
				break;
			case 'edit':
				$status = 'OK';
				$response = array();
				$now = getTimestamp(time());

				$id = $this->input->post('id');
				$companyId = $this->psecurity->getUserLoggedIn('company');
				$name = $this->input->post('name');
				$description = $this->input->post('description');
				$marketings = $this->input->post('marketings');

				if ($id == false) {
					$status = 'Failed';
					$response['errors'][] = 'Identifier Required!';
				}
				if ($name == false || $name == '') {
					$status = 'Failed';
					$response['errors'][] = 'Name Required!';
				} else {
					$dupName = $this->db->query('
						SELECT *
						FROM '.TBL_FORMS.'
						WHERE id <> '.$id.' AND
							  lower(name) = '.$this->db->escape(strtolower($name)).' AND
							  companyId = '.$this->db->escape($companyId).'
					');

					if ($dupName->num_rows() > 0) {
						$status = 'Failed';
						$response['errors'][] = 'Nama Sudah Terdaftar!';
					}
				}
				if ($marketings == false) {
					$marketings = [];
				}

				if ($status == 'OK') {
					$data = array(
						'name' 			=> $name,
						'description'	=> $description,
						'updatedAt' 	=> $now,
						'createdAt' 	=> $now,
					);

					if ($this->db->update(TBL_FORMS, $data, array('id' => $id))) {
						$response['js'] = '
							window.location = "'.site_url('form').'";
						';
					}
				}

				echo $this->build_json($status, $response);
				break;
			case 'publish':
				$status = 'OK';
				$response = array();
				$formId = $this->input->post("id");
				if ($formId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				} else {
					// Check form Item if it can be published
					$form = $this->form_factory->getOne($formId);
					if ($form->havePage) {
						$pages = $this->page_factory->get($formId);
						if (!$pages || sizeof($pages) < 2) {
							$status = 'Failed';
							$response['errors'][] = 'Project MUST have at LEAST 2 PAGES';
						} else {
							foreach ($pages as $page) {
								$items = $this->form_item_factory->get($formId, $page->id);
								if (!$items) {
									$status = 'Failed';
									$response['errors'][] = 'Page ['.$page->name.'] MUST contain at LEAST 1 Item';
								}
							}
						}
					} else {
						$items = $this->form_item_factory->get($formId, 0);
						if (!$items) {
							$status = 'Failed';
							$response['errors'][] = 'Project MUST contain at LEAST 1 Item';
						}
					}
				}

				if ($status == "OK") {
					if ($this->db->update(TBL_FORMS, ['published' => 1], ['id' => $formId])) {
						$js = '
							window.location.replace("'.site_url('form').'");
						';
						$response['js'] = $js;
					} else {
						$status = 'Failed';
						$response['errors'] = 'Publish failed';
					}
				}
				echo $this->build_json($status, $response);
				break;
		}
	}

	public function page()
	{
		$this->load->library(array('form_factory', 'page_factory', 'form_builder'));
		$status = "OK";
		$response = array();
		$action = $this->input->post("action");
		$now = getTimestamp(time());

		switch ($action) {
			case 'add':
				$formId = $this->input->post("id");
				if ($formId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}

				if ($status == "OK") {
					$input = '<div class="col-md-8" style="padding-left:0px; padding-right:0px;">';
					$input .= '<input id="new_page_name" type="text" placeholder="Page Name" class="form-control" />';
					$input .= '</div>';
					$input .= '<div class="col-md-4" style="margin-top:5px;">';
					$input .= '<a id="page_save_btn" class="confirm-button" data-placement="top" data-original-title="Save" data-toggle="tooltip"><i class="fa fa-check"></i></a>';
					$input .= '<a id="page_cancel_btn" data-placement="top" class="delete-button" data-original-title="Cancel" data-toggle="tooltip"><i class="fa fa-times"></i></a>';
					$input .= '</div>';

					$input .= '<div class="col-md-12" style="margin-top:10px; margin-bottom:10px; border-bottom:1px solid #BFBFBF; width:100%;"></div>';

					$js = "$('#form_page_input').html('".$input."');";
					$js .= "$('#form_page_input input').focus();";
					$js .= "$('#form_page_input').css({
								'padding-top': '10px',
								'padding-bottom': '10px',
								'margin-bottom': '10px'
							});";

					$js .= "$('#page_save_btn').on('click', function() {
								var post = {
					                'id'    : ".$formId.",
					                'action': 'save',
					                'name'	: $('#new_page_name').val().trim()
					            };
					            trackplus.request.ajax_request('page', post);
							});";

					$js .= "$('#page_cancel_btn').on('click', function() {
								console.log('cancel button clicked!');
								$('#form_page_input').empty();
								$('#form_page_input').removeAttr('style');
								$('#reorderPageWrapper').removeClass('hidden');
								$('#pageContainer div.action-btn').removeClass('hidden');
							});";

					$js .= "
						$('#reorderPageWrapper').addClass('hidden');
						$('#saveReorderPage').addClass('hidden');
				        $('#reorderPage').removeClass('hidden');
				        $('#pageContainer div.action-btn').addClass('hidden');
					";

					$response["js"] = $js;
				}
				break;
			case 'save':
				$formId = $this->input->post("id");
				$pageName = trim($this->input->post("name"));
				if ($formId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}
				if ($pageName == false || $pageName == "") {
					$status = "Failed";
					$response["errors"][] = "Page Name required!";
				}
				if ($this->page_factory->isDuplicatedName($formId, $pageName)) {
					$status = "Failed";
					$response["errors"][] = "Duplicate Page Name!";
				}

				if ($status == "OK") {
					// Save Page
					$sequence = $this->page_factory->getMaxSequence($formId);
					$page = array(
							'formId' 	=> $formId,
							'name'		=> $pageName,
							'sequence'	=> $sequence,
							'createdAt'	=> $now,
							'updatedAt'	=> $now
						);

					$pageId = $this->page_factory->save($page);
					if ($pageId) {
						$pageRow = $this->form_builder->renderPageRow($formId, $pageId);
						$js = "
							$('#form_page_input').empty();
							$('#pageContainer').append('".addslashes($pageRow)."');
							$('#reorderPageWrapper').removeClass('hidden');
							$('#pageContainer div.action-btn').removeClass('hidden');
						";
						$js .= printDomreadyJS(false);
						$response['js'] = $js;
					} else {
						$status = "failed";
						$response["errors"][] = "Failed to create page";
					}
				}
				break;
			case 'edit':
				$formId = $this->input->post("pid");
				$pageId = $this->input->post("id");
				$pageName = trim($this->input->post("name"));
				if ($formId == false || $pageId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}
				if ($pageName == false || $pageName == "") {
					$status = "Failed";
					$response["errors"][] = "Name required!";
				}
				if ($this->page_factory->isDuplicatedName($formId, $pageName, $pageId)) {
					$status = "Failed";
					$response["errors"][] = "Duplicate Page Name!";
				}

				if ($status == "OK") {
					// Update Page
					$page = array('name' => $pageName);

					if ($this->page_factory->update($pageId, $page)) {
						$pageRow = $this->form_builder->renderPageRow($formId, $pageId);
						$optsHtml = $this->form_builder->renderFormOptionButton($formId, $pageId);
						$itemsHtml = $this->form_builder->renderItemsHtml($formId, $pageId);
						$reorderItemsHtml = $this->form_builder->renderReorderItemsHtml($formId, $pageId);

						$js = '
							$("#page_'.$pageId.'_row").replaceWith("'.addslashes($pageRow).'");
							$("#pageName").html("<h4>'.$pageName.'</h4>");
							$("#pageName").css("border-bottom", "1px solid #BFBFBF");
							$("#formItemBtnContainer").html("'.addslashes($optsHtml).'");
							$("#reorderItemWrapper").remove();
							$("'.addslashes($reorderItemsHtml).'").insertBefore("#formDisplay");
							$("#formDisplay").html("'.addslashes($itemsHtml).'");
						';
						$js .= printDomreadyJS(false);
						$response['js'] = $js;
					} else {
						$status = "failed";
						$response["errors"][] = "Failed to create page";
					}
				}
				break;
			case 'delete':
				$pageId = $this->input->post("id");

				if ($pageId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}

				if ($status == "OK") {
					$page = array('deletedAt' => $now);
					if ($this->page_factory->delete($pageId, $page)) {
						$js = '
							$("#page_'.$pageId.'_row").remove();
						';
						$response['js'] = $js;
					} else {
						$status = "failed";
						$response["errors"][] = "Failed to delete page";
					}
				}
				break;
			case 'reorder':
				$formId = $this->input->post("id");
				if ($formId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}

				if ($status == "OK") {
					$sequences = $this->input->post("sequence");
					if ($sequences) {
						foreach ($sequences as $idx => $pageId) {
							$this->page_factory->update($pageId, array('sequence' => ($idx + 1)));
						}
					}

					$js = '
						$("#pageContainer").sortable("destroy");
						$("#saveReorderPage").addClass("hidden");
				        $("#reorderPage").removeClass("hidden");
				        $("#pageContainer div.action-btn").removeClass("hidden");
					';
					$response['js'] = $js;
				}
				break;
			case 'load_page_form':
				$pageId = $this->input->post("id");
				$formId = $this->input->post("pid");
				if ($formId == false || $pageId == false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}

				if ($status == "OK") {
					$page = $this->page_factory->getOne($pageId);
					$optsHtml = $this->form_builder->renderFormOptionButton($formId, $pageId);
					$itemsHtml = $this->form_builder->renderItemsHtml($formId, $pageId);
					$reorderItemsHtml = $this->form_builder->renderReorderItemsHtml($formId, $pageId);

					$js = '
						$("#pageName").html("<h4>'.$page->name.'</h4>");
						$("#pageName").css("border-bottom", "1px solid #BFBFBF");
						$("#formItemBtnContainer").html("'.addslashes($optsHtml).'");
						$("#reorderItemWrapper").remove();
						$("'.addslashes($reorderItemsHtml).'").insertBefore("#formDisplay");
						$("#formDisplay").html("'.addslashes($itemsHtml).'");
					';

					$js .= printDomreadyJS(false);
					$response['js'] = $js;
				}
				break;
			default:
				break;
		}

		echo $this->build_json($status, $response);
	}

	public function form_item()
	{
		$this->load->library(array('form_factory', 'page_factory', 'form_item_factory', 'form_builder'));
		$status = "OK";
		$response = array();
		$action = $this->input->post("action");
		$now = getTimestamp(time());

		switch ($action) {
			case 'show_blank_option':
				$formId = $this->input->post('id');
				$pageId = $this->input->post('pid');
				$type = trim($this->input->post('type'));

				if ($formId == false || $pageId === false) {
					$status = "failed";
					$response["errors"][] = "Identifier required!";
				}
				if ($type == false || $type == "") {
					$status = "failed";
					$response["errors"][] = "Type required!";
				}

				if ($status == "OK") {
					$js = $this->form_builder->renderOptModalJS($formId, $pageId, $type);
					$response['js'] = $js;
				}
				break;
			case 'show_option':
				$formId = $this->input->post('formId');
				$pageId = $this->input->post('pageId');
				$itemId = $this->input->post('itemId');
				$type = trim($this->input->post('type'));

				if ($formId == false || $pageId === false || $itemId === false) {
					$status = "failed";
					$response["errors"][] = "Identifier required!";
				}
				if ($type == false || $type == "") {
					$status = "failed";
					$response["errors"][] = "Type required!";
				}

				if ($status == "OK") {
					$js = $this->form_builder->renderOptModalJS($formId, $pageId, $type, $itemId);
					$response['js'] = $js;
				}
				break;
			case 'save':
				$formId = $this->input->post('formId');
				$pageId = $this->input->post('pageId');
				$itemId = $this->input->post('itemId');
				$type = trim($this->input->post('type'));
				$itemName = trim($this->input->post('itemName'));
				$itemCaption = trim($this->input->post('itemCaption'));
				// $itemOrder = $this->input->post('itemOrder');
				$isCompulsory = $this->input->post('isCompulsory');
				$options = $this->input->post('options');
				$dataStore = array();

				if ($formId == false || $pageId === false || $itemId === false) {
					$status = "failed";
					$response["errors"][] = "Identifier required!";
				}
				if ($type == false || $type == "") {
					$status = "failed";
					$response["errors"][] = "Type required!";
				}
				if ($itemName == false || $itemName == "") {
					$status = "failed";
					$response["errors"][] = "Item Name required!";
				}
				if ($this->form_item_factory->isDuplicatedName($formId, $pageId, $itemName, $itemId)) {
					$status = "failed";
					$response["errors"][] = "Duplicate Item Name!";
				}
				if ($isCompulsory === false) {
					$status = "failed";
					$response["errors"][] = "Is Compulsory required!";
				}
				if ($type == "checkbox" || $type == "radio" || $type == "dropdown") {
					if ($options != false) {
						$optionArr = explode("\n", $options);
						foreach ($optionArr as $value) {
							if (trim($value) != "") {
								$dataStore['values'][] = trim($value);
							}
						}
						if (sizeof($dataStore['values']) < 2) {
							$status = "failed";
							$response["errors"][] = "Options MUST have at LEAST 2 values!";
						}
					} else {
						$status = "failed";
						$response["errors"][] = "Options required!";
					}
				}

				if ($status == "OK") {

					$response['dataStore'] = $dataStore;

					$item = array(
							'formId' 		=> $formId,
							'pageId' 			=> $pageId,
							'name' 				=> $itemName,
							'caption' 			=> $itemCaption,
							'type' 				=> $type,
							'isCompulsory'		=> $isCompulsory,
							'dataStore'			=> serialize($dataStore),
							'visibilityArray'	=> serialize(array())
						);

					if ($itemId == 0) {
						// New
						$item['createdAt'] = $now;
						$item['updatedAt'] = $now;
						$item['sequence'] = $this->form_item_factory->getMaxSequence($formId, $pageId);

						if ($this->form_item_factory->save($item)) {
							$itemsHtml = $this->form_builder->renderItemsHtml($formId, $pageId);
							$js = '$("#formDisplay").html("'.addslashes($itemsHtml).'");';
							$js .= '$("#formItemOptionsModal").modal("hide");';
							$js .= printDomreadyJS(false);
							$response['js'] = $js;
						} else {
							$status = "failed";
							$response["errors"][] = "Failed to save item!";
						}
					} else {
						// Update
						if ($this->form_item_factory->update($itemId, $item)) {
							$itemsHtml = $this->form_builder->renderItemsHtml($formId, $pageId);
							$js = '$("#formDisplay").html("'.addslashes($itemsHtml).'");';
							$js .= '$("#formItemOptionsModal").modal("hide");';
							$js .= printDomreadyJS(false);
							$response['js'] = $js;
						} else {
							$status = "failed";
							$response["errors"][] = "Failed to save item!";
						}
					}

					$response['item'] = $item;
				}
				break;
			case 'delete':
				$formId = $this->input->post('formId');
				$pageId = $this->input->post('pageId');
				$itemId = $this->input->post('itemId');

				if ($formId == false || $pageId === false || $itemId === false) {
					$status = "failed";
					$response["errors"][] = "Identifier required!";
				}

				if ($status == "OK") {
					if ($this->form_item_factory->delete($itemId, false)) {
						$js = '
							$("#item_'.$itemId.'_row").remove();
						';
						$response['js'] = $js;
					}
				}
				break;
			case 'reorder':
				$formId = $this->input->post("id");
				$pageId = $this->input->post("pageId");
				if ($formId == false || $pageId === false) {
					$status = "Failed";
					$response["errors"][] = "Identifier required!";
				}

				if ($status == "OK") {
					$sequences = $this->input->post("sequence");
					foreach ($sequences as $idx => $itemId) {
						$this->form_item_factory->update($itemId, array('sequence' => ($idx + 1)));
					}

					$js = '
						$("#formDisplay").sortable("destroy");
						$("#saveReorderItem").addClass("hidden");
				        $("#reorderItem").removeClass("hidden");
					';
					$response['js'] = $js;
				}
				break;
			default:
				break;
		}
		echo $this->build_json($status, $response);
	}
}
