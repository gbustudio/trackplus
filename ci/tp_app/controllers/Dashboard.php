<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends TP_Controller {

	function __construct()
	{
		parent::__construct('Dashboard');
	}

	public function index()
	{
		$title = 'Dashboard';

		$data = [];
		/*$countQuery = $this->db->query("
			SELECT COUNT(distinct P.name) AS PROJECT, COUNT(distinct m.email) AS MARKETING
			FROM ".TBL_PROJECTS." P, ".TBL_MARKETINGS." M
			WHERE p.deletedAt IS NULL AND m.deletedAt IS NULL
		");

		if ($countQuery->num_rows() > 0) {
			$count = $countQuery->row();
			$projectCount = $count->PROJECT;
			$marketingCount = $count->MARKETING;
		}

		$activeQuery = $this->db->query("
			SELECT COUNT(published) AS ACTIVE FROM ".TBL_PROJECTS."
			WHERE published=1 and deletedAt IS NULL
		");

		if ($activeQuery->num_rows() > 0) {
			$active = $activeQuery->row();
			$activeProject = $active->ACTIVE;
		}

		$activeMarketingQuery = $this->db->query("
			SELECT COUNT(projectId) AS ACTIVEMARKETING FROM ".TBL_MARKETINGS."
			WHERE projectId=1 and deletedAt IS NULL
		");
		if ($activeMarketingQuery->num_rows() > 0) {
			$activeMarketing = $activeMarketingQuery->row();
			$activeMarketingCount = $activeMarketing->ACTIVEMARKETING;
		}

		$data = array(
				"activeProject" 	=> $activeProject,
				"projectCount"		=> $projectCount,
				"activeMarketing"	=> $activeMarketingCount,
				"marketingCount"	=> $marketingCount
			);*/

		echo $this->pagebuilder->renderPage('dashboard2', $title, $data, true);
	}

}