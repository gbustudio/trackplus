<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends TP_Controller
{
	function __construct()
	{
		parent::__construct('Form-Api');
		$this->load->library(array('form_builder', 'marketing_factory'));
	}

	public function Submission($marketingId)
	{
		$marketing = $this->marketing_factory->getOne($marketingId);
		if ($marketing) {
			$data['form'] = $this->form_builder->renderSubmissionForm($marketingId, 3);
			$data['style'] = $this->load->view('form-mobile/theme/default', '', true);
			$this->load->view('form-mobile/default', $data);
		}
	}

	public function Submit()
	{
		$now = getTimestamp(time());
		$status = 'OK';
		$response = [];

		$marketingId = $this->input->post('marketing');
		$formId = $this->input->post('form');

		if ($marketingId == false || $formId == false) {
			$status = 'Failed';
			$response['message'] = 'Identifier Required!';
		}

		$submission = $this->input->post('item');
		if (!$submission) {
			$submission = [];
		}

		if ($status == 'OK') {
			// Save submission
			$data = [
				'marketingId'	=> $marketingId,
				'formId'		=> $formId,
				'submission'	=> serialize($submission),
				'updatedAt' 	=> $now,
				'createdAt' 	=> $now
			];

			if ($this->db->insert(TBL_FORM_SUBMISSIONS, $data)) {
				redirect(site_url('api/form/finish'));
			} else {
				$status = 'Failed';
				$response['message'] = 'Failed to save submission!';
			}
		}
	}

	public function finish()
	{
		echo 'Submission successfuly saved!';
	}
}