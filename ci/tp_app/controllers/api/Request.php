<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends TP_Controller
{
	function __construct()
	{
		parent::__construct('Form-Api');
		$this->load->library(array('form_builder', 'marketing_factory', 'announcement_factory'));
	}

	private function build_json($status, $message, $output=array())
	{
		$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array_merge(array('status' => $status, 'message' => $message), $output)));
	}

	public function index()
	{
		$status = 200;
		$message = '';
		$data = array();

		$tag = $this->input->post("TAG");

		if ($tag == false) {
			$status = 100;
			$message = 'Request denied: illegal action';
		}

		if ($status == 200) {
			$tag = camelize($tag);
			if (method_exists($this, $tag)) {
				list($status, $message, $data) = $this->$tag();
			} else {
				$status = 100;
				$message = 'Request denied: illegal action';
			}
		}

		return $this->build_json($status, $message, $data);
	}

	private function authenticate()
	{
		$status = 200;
		$message = '';
		$data = array();

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$platform = $this->input->post('devicePlatform');
		$deviceId = $this->input->post('deviceId');
		$registrationId = $this->input->post('registrationId');

		if ($platform == false || $platform == '') {
			$status = 'Failed';
			$message = 'Device platform required';
		}
		if ($password == false) {
			$status = 'Failed';
			$message = 'Password required';
		}
		if ($email == false) {
			$status = 'Failed';
			$message = 'Email required';
		}

		if ($status == 200) {
			if ($this->psecurity->authenticateMarketing($email, $password)) {
				$marketing = $this->marketing_factory->get4Mobile($email);
				if ($marketing) {
					if (is_null($marketing->deletedAt) || $deletedAt == "") {
						$message = 'Authentication success, you are good to go.';
						$data['marketing'] = [
							'marketingId' => $marketing->id,
							'companyId'   => $marketing->companyId,
							'companyName' => $marketing->companyName,
							'taskId'      => '1',
							'email'       => $marketing->email,
							'name'        => $marketing->name,
							'address'     => $marketing->address,
							'phone'       => $marketing->phone
						];
					} else {
						$status = 100;
						$message = 'Authentication failed: your account has been deactivated, please contact your administrator';
					}
				} else {
					$status = 100;
					$message = 'Authentication failed: account not found';
				}
			} else {
				$status = 100;
				$message = 'Authentication failed: wrong email or password';
			}
		}

		return array($status, $message, array('data' => $data));
	}

	private function logout()
	{
		$status = 200;
		$message = '';
		$data = array();

		$marketingId = $this->input->post('marketingId');

		if ($marketingId == false) {
			$status = 100;
			$message = 'Missing token: Marketing ID';
		}

		if ($status == 200) {
			// TODO: Update marketing table
			$message = 'Successfully logout';
		}
		return array($status, $message, array('data' => $data));
	}

	private function getAnnouncements()
	{
		$status = 200;
		$message = '';
		$data = array();

		$companyId = $this->input->post('companyId');
		$taskId = $this->input->post('taskId');
		$marketingId = $this->input->post('marketingId');

		$tokens = [];
		if ($companyId == false) {
			$status = 100;
			$tokens[] = 'Company ID';
		}
		if ($taskId == false) {
			$status = 100;
			$tokens[] = 'Task ID';
		}
		if ($marketingId == false) {
			$status = 100;
			$tokens[] = 'Marketing ID';
		}

		if ($status == 200) {
			$message = 'Successfully get all announcements.';
			$announcements = $this->announcement_factory->get4Mobile($companyId);
			if ($announcements == false) {
				$data['announcements'] = [];
			} else {
				foreach ($announcements as $announcement) {
					$data['announcements'][] = [
						'id'      => $announcement->id,
						'title'   => $announcement->title,
						'content' => $announcement->content
					];
				}
			}
		} else {
			$message = 'Missing token: ';
			foreach ($tokens as $key => $token) {
				if ($key > 0) {
					$message .= ', ';
				}
				$message .= $token;
			}
		}

		return array($status, $message, array('data' => $data));
	}

	private function setLocationLog()
	{
		$status = 200;
		$message = '';
		$data = array();

		$marketingId = $this->input->post('marketingId');
		$deviceId = $this->input->post('deviceId');
		$latitude = $this->input->post('latitude');
		$longitude = $this->input->post('longitude');

		$tokens = [];
		if ($marketingId == false) {
			$status == 100;
			$tokens[] = 'Marketing ID';
		}
		if ($latitude == false) {
			$status == 100;
			$tokens[] = 'Latitude';
		}
		if ($longitude == false) {
			$status == 100;
			$tokens[] = 'Longitude';
		}

		if ($status == 200) {
			$message = 'Successfully save marketing location';
		} else {
			$message = 'Missing token: ';
			foreach ($tokens as $key => $token) {
				if ($key > 0) {
					$message .= ', ';
				}
				$message .= $token;
			}
		}

		return array($status, $message, array('data' => $data));
	}
}