<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends TP_Controller
{
	function __construct()
	{
		parent::__construct('Form-Api');
		$this->load->library(array('form_builder', 'marketing_factory', 'announcement_factory'));
	}

	private function build_json($status, $message, $output=array())
	{
		$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode($output));
	    			 // ->set_output(json_encode(array_merge(array('status' => $status, 'message' => $message), $output)));
	}

	public function index()
	{
		$status = 200;
		$message = '';

		$data = [
			'URL' => site_url('api/request'),
			'COMMON RETURN' => [
				'status' => [
					100 => 'Failed',
					200 => 'OK'
				],
				'message' => 'Readable message for human',
				'data'	=> 'Contains data if OK and empty array if Failed'
			],
			'Form Submission' => [
				'DESCRIPTION' => 'Show submission form on webview',
				'URL' => site_url('api/form/submission/{marketingId}')
			],
			'Announcement' => [
				'DESCRIPTION' => 'Show announcement on webview',
				'URL' => site_url('api/announcement/show/{marketingId}')
			],
			'Authentication / User Login' => [
				'DESCRIPTION' => 'Authenticate user credential',
				'KEY' => [
					'TAG'            => 'authenticate',
					'email'          => 'Marketing Email',
					'password'       => 'Marketing Password',
					'devicePlatform' => 'Device Platform (iOS/Android)',
					'deviceId'       => 'iOS or Android Device ID (can be null for now)',
					'registrationId' => 'GCM Registration ID (can be null for now)'
				],
				'RETURN' => [
					'marketingId',
					'companyId',
					'companyName',
					'taskId',
					'email',
					'name',
					'address',
					'phone'
				]
			],
			'User Logout' => [
				'DESCRIPTION' => 'Remove user login on server side, so user can login on another device',
				'KEY' => [
					'TAG'         => 'logout',
					'marketingId' => 'Marketing ID',
					'deviceId'    => 'iOS or Android Device ID (can be null for now)',
				],
				'RETURN' => []
			],
			'Announcements' => [
				'DESCRIPTION' => 'Get all published announcements',
				'KEY' => [
					'TAG'         => 'get_announcements',
					'companyId'   => 'Company ID',
					'taskId'      => 'Task ID',
					'marketingId' => 'Marketing ID',
					'deviceId'    => 'iOS or Android Device ID (can be null for now)',
				],
				'RETURN' => [
					'id',
					'title',
					'content'
				]
			],
			'Location Log' => [
				'DESCRIPTION' => 'Save user current location (In-Development)',
				'KEY' => [
					'TAG'         => 'set_location_log',
					'marketingId' => 'Marketing ID',
					'deviceId'    => 'iOS or Android Device ID (can be null for now)',
					'latitude'    => 'latitude',
					'longitude'   => 'longitude'
				],
				'RETURN' => []
			],
		];
		return $this->build_json($status, $message, $data);
	}

}