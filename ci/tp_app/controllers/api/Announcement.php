<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends TP_Controller
{
	function __construct()
	{
		parent::__construct('Announcement-Api');
		$this->load->library(array('form_builder', 'marketing_factory', 'announcement_factory'));
	}

	public function show($marketingId)
	{
		$marketing = $this->marketing_factory->getOne($marketingId);
		// var_dump($marketing); exit;
		if ($marketing) {
			$announcements = $this->announcement_factory->get4Mobile($marketing->companyId);
			$data['announcements'] = $announcements;
			$data['style'] = $this->load->view('mobile/theme/default', '', true);

			$js = 'trackplus.announcement.init();';
			addDomreadyJS($js);

			$this->load->view('mobile/announcement/default', $data);
		}
		else {
			show_404();
		}
	}
}