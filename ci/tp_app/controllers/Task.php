<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends TP_Controller {

	function __construct()
	{
		parent::__construct('Task');
	}

	public function index()
	{
		$title = 'Task';
		echo $this->pagebuilder->renderPage('task/main', $title, array(), true);
	}

	public function populateTableJS() {
		$companyId = $this->psecurity->getUserLoggedIn('company');
		$query = $this->db->query("
				SELECT T.id ID, T.name Name
				FROM ".TBL_TASKS." T
				WHERE M.companyId = ".$this->db->escape($companyId)." AND
			");

		$fields = $query->list_fields();
		$columns = array();

		foreach ($fields as $field) {
			if ($field != "ID") {
				$columns[] = array("title" => $field);
			} else {
				$columns[] = array("title" => "No");
			}
		}
		$columns[] = array("title" => "Action");

		$data = array();

		if ($query->num_rows() > 0) {
			$marketings = $query->result();

			foreach ($marketings as $key => $marketing) {
				$marketing->No = "";
				$marketing->Action = '
					<div class="no-margin no-padding table-option-wrapper">
					<a href="'.site_url('marketing/manage/edit/'.$marketing->ID).'" class="table-option border">Edit</a>
					<a class="table-option delete-row border" onclick="trackplus.marketing.delete_marketing('.$marketing->ID.', $(this))">Delete</a>
					</div>
				';
				$temp = array(
						$marketing->No,
						$marketing->Email,
						$marketing->Name,
						$marketing->Address,
						$marketing->Phone,
						$marketing->Action
					);

				array_push($data, $temp);
			}
		}

		// Table JS
		$js = '
			var table = $("#marketing_table").DataTable({
				"data": '.json_encode($data).',
	        	"columns": '.json_encode($columns).',
	        	"columnDefs": [{
		            "searchable": false,
		            "orderable": false,
		            "targets": 0
		        }],
		        "order": [[ 1, "asc" ]],
	        	"scrollY": 200,
	        	"scrollX": true,
	        	"language": {
			        "emptyTable": "No Data Found"
			    }
			});

			table.on("order.dt search.dt", function()
			{
		        table.column(0, {search:"applied", order:"applied"}).nodes().each(function(cell, i) {
		            cell.innerHTML = i+1;
		        });
		    }
		    ).draw();
		';

		return $js;
	}

}