<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dev90d5cd19f1807d85f46314a892b61755 extends TP_Controller {

	function __construct()
	{
		parent::__construct('Developer');
	}

	public function index() {
		echo "test";
	}

	public function isDupSlug($companyId, $slug) {
		$query = $this->db->query('SELECT *
								   FROM '.TBL_COMPANIES.'
								   WHERE id <> '.$this->db->escape($companyId).' AND
								   		 lower(slug) = '.$this->db->escape(strtolower($slug)).'
							');

		if ($query->num_rows() > 0) {
			return true;
		}
		return false;
	}

	public function isDupName($companyId, $name) {
		$query = $this->db->query('SELECT *
								   FROM '.TBL_COMPANIES.'
								   WHERE id <> '.$this->db->escape($companyId).' AND
								   		 lower(name) = '.$this->db->escape(strtolower($name)).'
							');

		if ($query->num_rows() > 0) {
			return true;
		}
		return false;
	}

	public function createCompany() {
		$name = "Global's Bersama Utama";
		$slug = getSlugFromString($name).time();

		while($this->isDupSlug(0, $slug)) {
			$slug = getSlugFromString($name).time();
		}

		$now = getTimestamp(time());

		$query = $this->db->query('INSERT INTO '.TBL_COMPANIES.'(name, slug, updatedAt, createdAt) VALUES(
				'.$this->db->escape($name).',
				'.$this->db->escape($slug).',
				'.$this->db->escape($now).',
				'.$this->db->escape($now).'
			)');

		if ($query) {
			echo "Insert Sukses";
		} else {
			echo "Insert Gagal";
		}
	}

	public function isDupEmail($email) {
		$query = $this->db->query('SELECT *
								   FROM '.TBL_ADMINS.'
								   WHERE lower(email) = '.$this->db->escape(strtolower($email)).'
							');

		if ($query->num_rows() > 0) {
			return true;
		}
		return false;
	}

	public function createAdmin() {
		$companyId = 1;
		$email = 'user1@trackplus.com';
		$passw = 'admin123';
		$md5Passw = md5($passw);
		$pwdLen = strlen($passw);
		$encPass = $this->psecurity->encryptPassword($md5Passw, $pwdLen);
		$now = getTimestamp(time());

		$permission = array(
				
			);

		if (!$this->isDupEmail($email)) {
			$query = $this->db->query('INSERT INTO tpAdmins(companyId, email, password, pwdLen, permission, updatedAt, createdAt) VALUES(
					'.$this->db->escape($companyId).',
					'.$this->db->escape($email).',
					'.$this->db->escape($encPass).',
					'.$this->db->escape($pwdLen).',
					'.$this->db->escape(serialize(array())).',
					'.$this->db->escape($now).',
					'.$this->db->escape($now).'
				)');

			if ($query) {
				echo 'Insert Sukses';
			} else {
				echo "Insert Gagal";
			}
		} else {
			echo "Insert Gagal: Email Telah Terdaftar";
		}
	}

	public function sendEmail()
	{
		$config = array(
		    'protocol' 	=> 'smtp',
		    'smtp_host' => 'ssl://nippon.websitewelcome.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'henry@globalbersamautama.com',
		    //'smtp_user'	=> 'nippon.websitewelcome.com',
		    'smtp_pass' => 'Henry123',
		    'mailtype'  => 'html',
		    'charset'   => 'iso-8859-1',
		    'crlf'		=> "\r\n",
		    'newline'		=> "\r\n"
		);

		$this->load->library('email', $config);

		$this->email->from('henry@globalbersamautama.com', 'Trackplus');
		$this->email->reply_to('no-reply@trackplus.com', 'Do Not Reply');
		$users = array(
				'antonius.henry.89@gmail.com',
				// 'getarasalimojoyo@gmail.com',
				// 'vickry.ard@gmail.com'
			);
		$this->email->to($users);

		$this->email->subject('Email Test');

		$message = $this->load->view('email', '', true);
		$this->email->message($message);

		$this->email->send();
		echo $this->email->print_debugger();
	}

	public function testDuplicatePageName() {
		$this->load->library(array('page_factory'));
		var_dump($this->page_factory->isDuplicatedName(1, "Page 2"));
	}

	public function testMaxSequence() {
		$this->load->library(array('form_factory'));
		var_dump($this->form_factory->getMaxSequence(1, 1));
	}

	public function generatePassword()
	{
		$this->load->helper("string");
		echo random_string("alnum", 16);
	}

	public function submission()
	{
		$this->load->library(array('project_form_builder'));
		$data['form'] = $this->project_form_builder->renderSubmissionForm(1);
		$data['style'] = $this->load->view('form/theme/default', '', true);
		$this->load->view('form/default', $data);
	}

	public function summary()
	{
		$this->load->library(array('project_form_builder'));
		$table = $this->project_form_builder->renderSummary(1);
		$data['form'] = $table;
		$data['style'] = $this->load->view('form/theme/default', '', true);
		$this->load->view('form/default', $data);
		//$data['style'] = $this->load->view('form/theme/default', '', true);
		//$this->load->view('form/default', $data);
	}

	public function getAvailableMarketingId() {
		$this->load->library(array('project_factory'));
		$marketings = $this->project_factory->getAvailableMarketingsId(3);
		var_dump(array_search(3, $marketings));
	}

}
