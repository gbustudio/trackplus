<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends TP_Controller
{
	function __construct()
	{
		parent::__construct('Logs');
	}

	public function index()
	{
		$title = 'Log';
		echo $this->pagebuilder->renderPage('log/main', $title, array(), true);
	}
}