<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'dashboard';
$route['dev'] = 'Dev90d5cd19f1807d85f46314a892b61755';
$route['dev/(:any)'] = 'Dev90d5cd19f1807d85f46314a892b61755/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
